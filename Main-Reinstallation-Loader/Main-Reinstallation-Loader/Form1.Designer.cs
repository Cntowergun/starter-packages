﻿
namespace Main_Reinstallation_Loader
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.WindowsCreationDateLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.label1.Location = new System.Drawing.Point(204, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(386, 104);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome to Windows 10\r\nPlease wait while we get things started\r\n\r\nPowered by BigH" +
    "eadOS™";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(33, 154);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(719, 51);
            this.progressBar1.TabIndex = 1;
            // 
            // WindowsCreationDateLabel
            // 
            this.WindowsCreationDateLabel.AutoSize = true;
            this.WindowsCreationDateLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F);
            this.WindowsCreationDateLabel.Location = new System.Drawing.Point(125, 208);
            this.WindowsCreationDateLabel.Name = "WindowsCreationDateLabel";
            this.WindowsCreationDateLabel.Size = new System.Drawing.Size(254, 26);
            this.WindowsCreationDateLabel.TabIndex = 2;
            this.WindowsCreationDateLabel.Text = "Windows 10 Installation -";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 245);
            this.Controls.Add(this.WindowsCreationDateLabel);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label WindowsCreationDateLabel;
    }
}

