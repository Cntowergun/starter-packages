﻿namespace EverythingSetupper
{
    partial class SelectComponents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectComponents));
            this.SoftwareStoreOn = new System.Windows.Forms.PictureBox();
            this.SoftwareStoreOff = new System.Windows.Forms.PictureBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.InternetCheckerOn = new System.Windows.Forms.PictureBox();
            this.InternetCheckerOff = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.Office365On = new System.Windows.Forms.PictureBox();
            this.Office365Off = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.BitdefenderON = new System.Windows.Forms.PictureBox();
            this.BitdefenderOff = new System.Windows.Forms.PictureBox();
            this.ActivateWindowsOn = new System.Windows.Forms.PictureBox();
            this.ActivateWindowsOff = new System.Windows.Forms.PictureBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ComputerMonitorOn = new System.Windows.Forms.PictureBox();
            this.ComputerMonitorOff = new System.Windows.Forms.PictureBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.FileVaultOn = new System.Windows.Forms.PictureBox();
            this.FileVaultOff = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.SoftwareStoreOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoftwareStoreOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternetCheckerOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternetCheckerOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Office365On)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Office365Off)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BitdefenderON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BitdefenderOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActivateWindowsOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActivateWindowsOff)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComputerMonitorOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComputerMonitorOff)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FileVaultOn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FileVaultOff)).BeginInit();
            this.SuspendLayout();
            // 
            // SoftwareStoreOn
            // 
            this.SoftwareStoreOn.Image = global::EverythingSetupper.Properties.Resources.on;
            this.SoftwareStoreOn.Location = new System.Drawing.Point(247, 97);
            this.SoftwareStoreOn.Name = "SoftwareStoreOn";
            this.SoftwareStoreOn.Size = new System.Drawing.Size(100, 50);
            this.SoftwareStoreOn.TabIndex = 1;
            this.SoftwareStoreOn.TabStop = false;
            this.SoftwareStoreOn.Click += new System.EventHandler(this.SoftwareStoreOn_Click);
            // 
            // SoftwareStoreOff
            // 
            this.SoftwareStoreOff.Image = global::EverythingSetupper.Properties.Resources.off;
            this.SoftwareStoreOff.Location = new System.Drawing.Point(247, 97);
            this.SoftwareStoreOff.Name = "SoftwareStoreOff";
            this.SoftwareStoreOff.Size = new System.Drawing.Size(100, 50);
            this.SoftwareStoreOff.TabIndex = 0;
            this.SoftwareStoreOff.TabStop = false;
            this.SoftwareStoreOff.Click += new System.EventHandler(this.SoftwareStoreOff_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(383, 692);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(526, 45);
            this.button1.TabIndex = 2;
            this.button1.Text = "Done";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.Control;
            this.label1.Location = new System.Drawing.Point(445, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(634, 42);
            this.label1.TabIndex = 3;
            this.label1.Text = "Select Extra Windows Components";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.Control;
            this.label2.Location = new System.Drawing.Point(35, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(194, 31);
            this.label2.TabIndex = 4;
            this.label2.Text = "Software Store";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label3.ForeColor = System.Drawing.SystemColors.Control;
            this.label3.Location = new System.Drawing.Point(38, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(203, 104);
            this.label3.TabIndex = 5;
            this.label3.Text = resources.GetString("label3.Text");
            // 
            // InternetCheckerOn
            // 
            this.InternetCheckerOn.Image = global::EverythingSetupper.Properties.Resources.on;
            this.InternetCheckerOn.Location = new System.Drawing.Point(238, 438);
            this.InternetCheckerOn.Name = "InternetCheckerOn";
            this.InternetCheckerOn.Size = new System.Drawing.Size(100, 50);
            this.InternetCheckerOn.TabIndex = 7;
            this.InternetCheckerOn.TabStop = false;
            this.InternetCheckerOn.Click += new System.EventHandler(this.InternetCheckerOn_Click);
            // 
            // InternetCheckerOff
            // 
            this.InternetCheckerOff.Image = global::EverythingSetupper.Properties.Resources.off;
            this.InternetCheckerOff.Location = new System.Drawing.Point(238, 438);
            this.InternetCheckerOff.Name = "InternetCheckerOff";
            this.InternetCheckerOff.Size = new System.Drawing.Size(100, 50);
            this.InternetCheckerOff.TabIndex = 6;
            this.InternetCheckerOff.TabStop = false;
            this.InternetCheckerOff.Click += new System.EventHandler(this.InternetCheckerOff_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(38, 419);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(203, 104);
            this.label4.TabIndex = 9;
            this.label4.Text = "Speed test your internet or wifi\r\neach time you start up your\r\ncomputer to save t" +
    "ime by\r\nnot going to speedtest.net\r\nyourself\r\n\r\n*A popup will show up each time " +
    "you boot\r\nyour computer to test speeds.";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(35, 371);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(253, 31);
            this.label5.TabIndex = 8;
            this.label5.Text = "Internet Speed Test";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label6.ForeColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(38, 271);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(210, 65);
            this.label6.TabIndex = 11;
            this.label6.Text = "Includes:\r\n\r\nWord, Excel, Powerpoint, Sharepoint,\r\nOutlook, Skype, Teams (will po" +
    "pup evertime\r\nyou start your computer), Publisher, Access";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(35, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(256, 31);
            this.label7.TabIndex = 10;
            this.label7.Text = "Microsoft Office 365";
            // 
            // Office365On
            // 
            this.Office365On.Image = global::EverythingSetupper.Properties.Resources.on;
            this.Office365On.Location = new System.Drawing.Point(264, 286);
            this.Office365On.Name = "Office365On";
            this.Office365On.Size = new System.Drawing.Size(100, 50);
            this.Office365On.TabIndex = 13;
            this.Office365On.TabStop = false;
            this.Office365On.Click += new System.EventHandler(this.Office365On_Click);
            // 
            // Office365Off
            // 
            this.Office365Off.Image = global::EverythingSetupper.Properties.Resources.off;
            this.Office365Off.Location = new System.Drawing.Point(264, 286);
            this.Office365Off.Name = "Office365Off";
            this.Office365Off.Size = new System.Drawing.Size(100, 50);
            this.Office365Off.TabIndex = 12;
            this.Office365Off.TabStop = false;
            this.Office365Off.Click += new System.EventHandler(this.Office365Off_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label12.ForeColor = System.Drawing.SystemColors.Control;
            this.label12.Location = new System.Drawing.Point(390, 419);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(204, 117);
            this.label12.TabIndex = 25;
            this.label12.Text = resources.GetString("label12.Text");
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.SystemColors.Control;
            this.label13.Location = new System.Drawing.Point(397, 371);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(158, 31);
            this.label13.TabIndex = 24;
            this.label13.Text = "BitDefender";
            // 
            // BitdefenderON
            // 
            this.BitdefenderON.Image = global::EverythingSetupper.Properties.Resources.on;
            this.BitdefenderON.Location = new System.Drawing.Point(600, 438);
            this.BitdefenderON.Name = "BitdefenderON";
            this.BitdefenderON.Size = new System.Drawing.Size(100, 50);
            this.BitdefenderON.TabIndex = 23;
            this.BitdefenderON.TabStop = false;
            this.BitdefenderON.Click += new System.EventHandler(this.BitdefenderON_Click);
            // 
            // BitdefenderOff
            // 
            this.BitdefenderOff.Image = global::EverythingSetupper.Properties.Resources.off;
            this.BitdefenderOff.Location = new System.Drawing.Point(600, 438);
            this.BitdefenderOff.Name = "BitdefenderOff";
            this.BitdefenderOff.Size = new System.Drawing.Size(100, 50);
            this.BitdefenderOff.TabIndex = 22;
            this.BitdefenderOff.TabStop = false;
            this.BitdefenderOff.Click += new System.EventHandler(this.BitdefenderOff_Click);
            // 
            // ActivateWindowsOn
            // 
            this.ActivateWindowsOn.Image = global::EverythingSetupper.Properties.Resources.on;
            this.ActivateWindowsOn.Location = new System.Drawing.Point(669, 97);
            this.ActivateWindowsOn.Name = "ActivateWindowsOn";
            this.ActivateWindowsOn.Size = new System.Drawing.Size(100, 50);
            this.ActivateWindowsOn.TabIndex = 29;
            this.ActivateWindowsOn.TabStop = false;
            this.ActivateWindowsOn.Click += new System.EventHandler(this.ActivateWindowsOn_Click);
            // 
            // ActivateWindowsOff
            // 
            this.ActivateWindowsOff.Image = global::EverythingSetupper.Properties.Resources.off;
            this.ActivateWindowsOff.Location = new System.Drawing.Point(669, 97);
            this.ActivateWindowsOff.Name = "ActivateWindowsOff";
            this.ActivateWindowsOff.Size = new System.Drawing.Size(100, 50);
            this.ActivateWindowsOff.TabIndex = 28;
            this.ActivateWindowsOff.TabStop = false;
            this.ActivateWindowsOff.Click += new System.EventHandler(this.ActivateWindowsOff_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label14.ForeColor = System.Drawing.SystemColors.Control;
            this.label14.Location = new System.Drawing.Point(390, 82);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(190, 65);
            this.label14.TabIndex = 27;
            this.label14.Text = "Automatically Activate Windows\r\n\r\nwithout installing other programs\r\n\r\n*Internet " +
    "is required to run this software\r\n";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.SystemColors.Control;
            this.label15.Location = new System.Drawing.Point(387, 30);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(229, 31);
            this.label15.TabIndex = 26;
            this.label15.Text = "Activate Windows";
            // 
            // ComputerMonitorOn
            // 
            this.ComputerMonitorOn.Image = global::EverythingSetupper.Properties.Resources.on;
            this.ComputerMonitorOn.Location = new System.Drawing.Point(669, 286);
            this.ComputerMonitorOn.Name = "ComputerMonitorOn";
            this.ComputerMonitorOn.Size = new System.Drawing.Size(100, 50);
            this.ComputerMonitorOn.TabIndex = 33;
            this.ComputerMonitorOn.TabStop = false;
            this.ComputerMonitorOn.Click += new System.EventHandler(this.ComputerMonitorOn_Click);
            // 
            // ComputerMonitorOff
            // 
            this.ComputerMonitorOff.Image = global::EverythingSetupper.Properties.Resources.off;
            this.ComputerMonitorOff.Location = new System.Drawing.Point(669, 286);
            this.ComputerMonitorOff.Name = "ComputerMonitorOff";
            this.ComputerMonitorOff.Size = new System.Drawing.Size(100, 50);
            this.ComputerMonitorOff.TabIndex = 32;
            this.ComputerMonitorOff.TabStop = false;
            this.ComputerMonitorOff.Click += new System.EventHandler(this.ComputerMonitorOff_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label16.ForeColor = System.Drawing.SystemColors.Control;
            this.label16.Location = new System.Drawing.Point(390, 271);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(235, 65);
            this.label16.TabIndex = 31;
            this.label16.Text = "Displays a popup when your computer\r\nis overheating, internet disconnects, IP cha" +
    "nged,\r\nand more stuff to come in the future.\r\n\r\n*Not all computers support overh" +
    "eat detection";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.SystemColors.Control;
            this.label17.Location = new System.Drawing.Point(387, 219);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(230, 31);
            this.label17.TabIndex = 30;
            this.label17.Text = "Computer Monitor";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.FileVaultOn);
            this.panel1.Controls.Add(this.FileVaultOff);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.ComputerMonitorOn);
            this.panel1.Controls.Add(this.ComputerMonitorOff);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.ActivateWindowsOn);
            this.panel1.Controls.Add(this.ActivateWindowsOff);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.BitdefenderON);
            this.panel1.Controls.Add(this.BitdefenderOff);
            this.panel1.Controls.Add(this.Office365On);
            this.panel1.Controls.Add(this.Office365Off);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.InternetCheckerOn);
            this.panel1.Controls.Add(this.InternetCheckerOff);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.SoftwareStoreOn);
            this.panel1.Controls.Add(this.SoftwareStoreOff);
            this.panel1.Location = new System.Drawing.Point(49, 83);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1305, 548);
            this.panel1.TabIndex = 34;
            // 
            // FileVaultOn
            // 
            this.FileVaultOn.Image = global::EverythingSetupper.Properties.Resources.on;
            this.FileVaultOn.Location = new System.Drawing.Point(1102, 97);
            this.FileVaultOn.Name = "FileVaultOn";
            this.FileVaultOn.Size = new System.Drawing.Size(100, 50);
            this.FileVaultOn.TabIndex = 37;
            this.FileVaultOn.TabStop = false;
            this.FileVaultOn.Click += new System.EventHandler(this.FileVaultOn_Click);
            // 
            // FileVaultOff
            // 
            this.FileVaultOff.Image = global::EverythingSetupper.Properties.Resources.off;
            this.FileVaultOff.Location = new System.Drawing.Point(1102, 97);
            this.FileVaultOff.Name = "FileVaultOff";
            this.FileVaultOff.Size = new System.Drawing.Size(100, 50);
            this.FileVaultOff.TabIndex = 36;
            this.FileVaultOff.TabStop = false;
            this.FileVaultOff.Click += new System.EventHandler(this.FileVaultOff_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(823, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 78);
            this.label8.TabIndex = 35;
            this.label8.Text = "Includes:\r\n\r\nSecurity Tools\r\nSecure File Vault\r\nGitHub-Large-Uploader\r\nMedia Crea" +
    "tion Tool";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.Control;
            this.label9.Location = new System.Drawing.Point(820, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(215, 62);
            this.label9.TabIndex = 34;
            this.label9.Text = "Security Tools\r\n(Recommended)";
            // 
            // SelectComponents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Name = "SelectComponents";
            this.Text = "SelectComponents";
            this.Load += new System.EventHandler(this.SelectComponents_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SoftwareStoreOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SoftwareStoreOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternetCheckerOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.InternetCheckerOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Office365On)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Office365Off)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BitdefenderON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BitdefenderOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActivateWindowsOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActivateWindowsOff)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComputerMonitorOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComputerMonitorOff)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FileVaultOn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FileVaultOff)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox SoftwareStoreOff;
        private System.Windows.Forms.PictureBox SoftwareStoreOn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox InternetCheckerOn;
        private System.Windows.Forms.PictureBox InternetCheckerOff;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox Office365On;
        private System.Windows.Forms.PictureBox Office365Off;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox BitdefenderON;
        private System.Windows.Forms.PictureBox BitdefenderOff;
        private System.Windows.Forms.PictureBox ActivateWindowsOn;
        private System.Windows.Forms.PictureBox ActivateWindowsOff;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.PictureBox ComputerMonitorOn;
        private System.Windows.Forms.PictureBox ComputerMonitorOff;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox FileVaultOn;
        private System.Windows.Forms.PictureBox FileVaultOff;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}