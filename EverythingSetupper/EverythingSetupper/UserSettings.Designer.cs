﻿namespace EverythingSetupper
{
    partial class UserSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CurrentComputerAndUsername = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CreateWindowsPassword = new System.Windows.Forms.TextBox();
            this.ConfirmWindowsPassword = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.AdminAccount = new System.Windows.Forms.CheckBox();
            this.ActivateWindows = new System.Windows.Forms.CheckBox();
            this.WindowsEdition = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.NewUsername = new System.Windows.Forms.TextBox();
            this.NewPassword = new System.Windows.Forms.TextBox();
            this.NewConfirmPassword = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.UserAccountListBox = new System.Windows.Forms.ListBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.GuestAccount = new System.Windows.Forms.CheckBox();
            this.label9 = new System.Windows.Forms.Label();
            this.NewWindowsUsername = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.StarterPackagesCheckBox = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // CurrentComputerAndUsername
            // 
            this.CurrentComputerAndUsername.AutoSize = true;
            this.CurrentComputerAndUsername.ForeColor = System.Drawing.Color.White;
            this.CurrentComputerAndUsername.Location = new System.Drawing.Point(27, 21);
            this.CurrentComputerAndUsername.Name = "CurrentComputerAndUsername";
            this.CurrentComputerAndUsername.Size = new System.Drawing.Size(231, 50);
            this.CurrentComputerAndUsername.TabIndex = 0;
            this.CurrentComputerAndUsername.Text = "Current Username:\r\nCurrent Computer Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(27, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(689, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Set a new windows password (Your current windows password will be deleted)";
            // 
            // CreateWindowsPassword
            // 
            this.CreateWindowsPassword.Location = new System.Drawing.Point(119, 140);
            this.CreateWindowsPassword.Name = "CreateWindowsPassword";
            this.CreateWindowsPassword.Size = new System.Drawing.Size(584, 30);
            this.CreateWindowsPassword.TabIndex = 2;
            this.CreateWindowsPassword.UseSystemPasswordChar = true;
            // 
            // ConfirmWindowsPassword
            // 
            this.ConfirmWindowsPassword.Location = new System.Drawing.Point(119, 201);
            this.ConfirmWindowsPassword.Name = "ConfirmWindowsPassword";
            this.ConfirmWindowsPassword.Size = new System.Drawing.Size(584, 30);
            this.ConfirmWindowsPassword.TabIndex = 3;
            this.ConfirmWindowsPassword.UseSystemPasswordChar = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(27, 143);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 25);
            this.label2.TabIndex = 4;
            this.label2.Text = "Create:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(18, 202);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 25);
            this.label3.TabIndex = 5;
            this.label3.Text = "Confirm:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1212, 694);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 43);
            this.button1.TabIndex = 6;
            this.button1.Text = "Continue";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.SystemColors.Control;
            this.label4.Location = new System.Drawing.Point(27, 260);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(215, 25);
            this.label4.TabIndex = 7;
            this.label4.Text = "More windows settings:";
            // 
            // AdminAccount
            // 
            this.AdminAccount.AutoSize = true;
            this.AdminAccount.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.AdminAccount.Location = new System.Drawing.Point(32, 299);
            this.AdminAccount.Name = "AdminAccount";
            this.AdminAccount.Size = new System.Drawing.Size(353, 29);
            this.AdminAccount.TabIndex = 8;
            this.AdminAccount.Text = "Enable Built-In Administrator Account";
            this.AdminAccount.UseVisualStyleBackColor = true;
            this.AdminAccount.CheckedChanged += new System.EventHandler(this.AdminAccount_CheckedChanged);
            // 
            // ActivateWindows
            // 
            this.ActivateWindows.AutoSize = true;
            this.ActivateWindows.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ActivateWindows.Location = new System.Drawing.Point(32, 343);
            this.ActivateWindows.Name = "ActivateWindows";
            this.ActivateWindows.Size = new System.Drawing.Size(193, 29);
            this.ActivateWindows.TabIndex = 9;
            this.ActivateWindows.Text = "Activate Windows:";
            this.ActivateWindows.UseVisualStyleBackColor = true;
            // 
            // WindowsEdition
            // 
            this.WindowsEdition.FormattingEnabled = true;
            this.WindowsEdition.Items.AddRange(new object[] {
            "Pro",
            "Home",
            "Education",
            "Enterprise"});
            this.WindowsEdition.Location = new System.Drawing.Point(231, 343);
            this.WindowsEdition.Name = "WindowsEdition";
            this.WindowsEdition.Size = new System.Drawing.Size(170, 33);
            this.WindowsEdition.TabIndex = 10;
            this.WindowsEdition.Text = "- Select Edition -";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.SystemColors.Control;
            this.label5.Location = new System.Drawing.Point(27, 394);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(247, 25);
            this.label5.TabIndex = 11;
            this.label5.Text = "Create more user accounts";
            // 
            // NewUsername
            // 
            this.NewUsername.Location = new System.Drawing.Point(133, 433);
            this.NewUsername.Name = "NewUsername";
            this.NewUsername.Size = new System.Drawing.Size(339, 30);
            this.NewUsername.TabIndex = 12;
            // 
            // NewPassword
            // 
            this.NewPassword.Location = new System.Drawing.Point(133, 487);
            this.NewPassword.Name = "NewPassword";
            this.NewPassword.Size = new System.Drawing.Size(339, 30);
            this.NewPassword.TabIndex = 13;
            this.NewPassword.UseSystemPasswordChar = true;
            // 
            // NewConfirmPassword
            // 
            this.NewConfirmPassword.Location = new System.Drawing.Point(133, 543);
            this.NewConfirmPassword.Name = "NewConfirmPassword";
            this.NewConfirmPassword.Size = new System.Drawing.Size(339, 30);
            this.NewConfirmPassword.TabIndex = 14;
            this.NewConfirmPassword.UseSystemPasswordChar = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.SystemColors.Control;
            this.label6.Location = new System.Drawing.Point(18, 433);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(108, 25);
            this.label6.TabIndex = 15;
            this.label6.Text = "Username:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.SystemColors.Control;
            this.label7.Location = new System.Drawing.Point(22, 487);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 25);
            this.label7.TabIndex = 16;
            this.label7.Text = "Password:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.SystemColors.Control;
            this.label8.Location = new System.Drawing.Point(38, 541);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(86, 25);
            this.label8.TabIndex = 17;
            this.label8.Text = "Confirm:";
            // 
            // UserAccountListBox
            // 
            this.UserAccountListBox.FormattingEnabled = true;
            this.UserAccountListBox.ItemHeight = 25;
            this.UserAccountListBox.Location = new System.Drawing.Point(43, 637);
            this.UserAccountListBox.Name = "UserAccountListBox";
            this.UserAccountListBox.Size = new System.Drawing.Size(423, 79);
            this.UserAccountListBox.TabIndex = 18;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(43, 579);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(422, 37);
            this.button2.TabIndex = 19;
            this.button2.Text = "Create";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(475, 678);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(110, 38);
            this.button3.TabIndex = 20;
            this.button3.Text = "Remove";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // GuestAccount
            // 
            this.GuestAccount.AutoSize = true;
            this.GuestAccount.ForeColor = System.Drawing.SystemColors.Control;
            this.GuestAccount.Location = new System.Drawing.Point(432, 299);
            this.GuestAccount.Name = "GuestAccount";
            this.GuestAccount.Size = new System.Drawing.Size(226, 29);
            this.GuestAccount.TabIndex = 21;
            this.GuestAccount.Text = "Enable Guest Account";
            this.GuestAccount.UseVisualStyleBackColor = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(768, 140);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(211, 50);
            this.label9.TabIndex = 22;
            this.label9.Text = "Set new username:\r\n(Default is \"Computer\")";
            // 
            // NewWindowsUsername
            // 
            this.NewWindowsUsername.Location = new System.Drawing.Point(971, 138);
            this.NewWindowsUsername.Name = "NewWindowsUsername";
            this.NewWindowsUsername.Size = new System.Drawing.Size(340, 30);
            this.NewWindowsUsername.TabIndex = 23;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.panel1.Location = new System.Drawing.Point(752, 82);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(594, 149);
            this.panel1.TabIndex = 24;
            // 
            // StarterPackagesCheckBox
            // 
            this.StarterPackagesCheckBox.AutoSize = true;
            this.StarterPackagesCheckBox.BackColor = System.Drawing.Color.Yellow;
            this.StarterPackagesCheckBox.Checked = true;
            this.StarterPackagesCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.StarterPackagesCheckBox.Enabled = false;
            this.StarterPackagesCheckBox.Location = new System.Drawing.Point(871, 689);
            this.StarterPackagesCheckBox.Name = "StarterPackagesCheckBox";
            this.StarterPackagesCheckBox.Size = new System.Drawing.Size(321, 54);
            this.StarterPackagesCheckBox.TabIndex = 25;
            this.StarterPackagesCheckBox.Text = "I agree to install Starter Packages\r\nafter windows setup";
            this.StarterPackagesCheckBox.UseVisualStyleBackColor = false;
            // 
            // UserSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.Controls.Add(this.StarterPackagesCheckBox);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.NewWindowsUsername);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.GuestAccount);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.UserAccountListBox);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.NewConfirmPassword);
            this.Controls.Add(this.NewPassword);
            this.Controls.Add(this.NewUsername);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.WindowsEdition);
            this.Controls.Add(this.ActivateWindows);
            this.Controls.Add(this.AdminAccount);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ConfirmWindowsPassword);
            this.Controls.Add(this.CreateWindowsPassword);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CurrentComputerAndUsername);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "UserSettings";
            this.Text = "UserSettings";
            this.Load += new System.EventHandler(this.UserSettings_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CurrentComputerAndUsername;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox CreateWindowsPassword;
        private System.Windows.Forms.TextBox ConfirmWindowsPassword;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox AdminAccount;
        private System.Windows.Forms.CheckBox ActivateWindows;
        private System.Windows.Forms.ComboBox WindowsEdition;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox NewUsername;
        private System.Windows.Forms.TextBox NewPassword;
        private System.Windows.Forms.TextBox NewConfirmPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ListBox UserAccountListBox;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.CheckBox GuestAccount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox NewWindowsUsername;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox StarterPackagesCheckBox;
    }
}