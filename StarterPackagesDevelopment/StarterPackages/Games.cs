﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Media;
using System.Net;
using System.Threading.Tasks;
using System.Windows.Forms;
using IWshRuntimeLibrary;
using StarterPackages.Properties;
using File = System.IO.File;

namespace StarterPackages
{
    public partial class Games : Form
    {
        private readonly string DefaultVeracryptEXEPath = "\"C:\\Program Files\\VeraCrypt\\VeraCrypt.exe\"";

        private bool Exit;
        private readonly string User = Environment.GetEnvironmentVariable("USERPROFILE");

        public Games()
        {
            InitializeComponent();
        }

        private async Task ShowNotification(string title, string message)
        {
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Caller.bat",
                "@if (@X)==(@Y) @end /* JScript comment\r\n@echo off\r\n\r\nsetlocal\r\ndel /q /f %~n0.exe >nul 2>&1\r\nfor /f \"tokens=* delims=\" %%v in ('dir /b /s /a:-d  /o:-n \"%SystemRoot%\\Microsoft.NET\\Framework\\*jsc.exe\"') do (\r\n   set \"jsc=%%v\"\r\n)\r\n\r\nif not exist \"%~n0.exe\" (\r\n    \"%jsc%\" /nologo /out:\"%~n0.exe\" \"%~dpsfnx0\"\r\n)\r\n\r\nif exist \"%~n0.exe\" ( \r\n    \"%~n0.exe\" %* \r\n)\r\n\r\n\r\nendlocal & exit /b %errorlevel%\r\n\r\nend of jscript comment*/\r\n\r\nimport System;\r\nimport System.Windows;\r\nimport System.Windows.Forms;\r\nimport System.Drawing;\r\nimport System.Drawing.SystemIcons;\r\n\r\n\r\nvar arguments:String[] = Environment.GetCommandLineArgs();\r\n\r\n\r\nvar notificationText=\"Warning\";\r\nvar icon=System.Drawing.SystemIcons.Hand;\r\nvar tooltip=null;\r\n//var tooltip=System.Windows.Forms.ToolTipIcon.Info;\r\nvar title=\"\";\r\n//var title=null;\r\nvar timeInMS:Int32=2000;\r\n\r\n\r\n\r\n\r\n\r\nfunction printHelp( ) {\r\n   print( arguments[0] + \" [-tooltip warning|none|warning|info] [-time milliseconds] [-title title] [-text text] [-icon question|hand|exclamation|аsterisk|application|information|shield|question|warning|windlogo]\" );\r\n\r\n}\r\n\r\nfunction setTooltip(t) {\r\n\tswitch(t.toLowerCase()){\r\n\r\n\t\tcase \"error\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Error;\r\n\t\t\tbreak;\r\n\t\tcase \"none\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.None;\r\n\t\t\tbreak;\r\n\t\tcase \"warning\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Warning;\r\n\t\t\tbreak;\r\n\t\tcase \"info\":\r\n\t\t\ttooltip=System.Windows.Forms.ToolTipIcon.Info;\r\n\t\t\tbreak;\r\n\t\tdefault:\r\n\t\t\t//tooltip=null;\r\n\t\t\tprint(\"Warning: invalid tooltip value: \"+ t);\r\n\t\t\tbreak;\r\n\t\t\r\n\t}\r\n\t\r\n}\r\n\r\nfunction setIcon(i) {\r\n\tswitch(i.toLowerCase()){\r\n\t\t //Could be Application,Asterisk,Error,Exclamation,Hand,Information,Question,Shield,Warning,WinLogo\r\n\t\tcase \"hand\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Hand;\r\n\t\t\tbreak;\r\n\t\tcase \"application\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Application;\r\n\t\t\tbreak;\r\n\t\tcase \"аsterisk\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Asterisk;\r\n\t\t\tbreak;\r\n\t\tcase \"error\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Error;\r\n\t\t\tbreak;\r\n\t\tcase \"exclamation\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Exclamation;\r\n\t\t\tbreak;\r\n\t\tcase \"hand\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Hand;\r\n\t\t\tbreak;\r\n\t\tcase \"information\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Information;\r\n\t\t\tbreak;\r\n\t\tcase \"question\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Question;\r\n\t\t\tbreak;\r\n\t\tcase \"shield\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Shield;\r\n\t\t\tbreak;\r\n\t\tcase \"warning\":\r\n\t\t\ticon=System.Drawing.SystemIcons.Warning;\r\n\t\t\tbreak;\r\n\t\tcase \"winlogo\":\r\n\t\t\ticon=System.Drawing.SystemIcons.WinLogo;\r\n\t\t\tbreak;\r\n\t\tdefault:\r\n\t\t\tprint(\"Warning: invalid icon value: \"+ i);\r\n\t\t\tbreak;\t\t\r\n\t}\r\n}\r\n\r\n\r\nfunction parseArgs(){\r\n\tif ( arguments.length == 1 || arguments[1].toLowerCase() == \"-help\" || arguments[1].toLowerCase() == \"-help\"   ) {\r\n\t\tprintHelp();\r\n\t\tEnvironment.Exit(0);\r\n\t}\r\n\t\r\n\tif (arguments.length%2 == 0) {\r\n\t\tprint(\"Wrong number of arguments\");\r\n\t\tEnvironment.Exit(1);\r\n\t} \r\n\tfor (var i=1;i<arguments.length-1;i=i+2){\r\n\t\ttry{\r\n\t\t\t//print(arguments[i] +\"::::\" +arguments[i+1]);\r\n\t\t\tswitch(arguments[i].toLowerCase()){\r\n\t\t\t\tcase '-text':\r\n\t\t\t\t\tnotificationText=arguments[i+1];\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-title':\r\n\t\t\t\t\ttitle=arguments[i+1];\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-time':\r\n\t\t\t\t\ttimeInMS=parseInt(arguments[i+1]);\r\n\t\t\t\t\tif(isNaN(timeInMS))  timeInMS=2000;\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-tooltip':\r\n\t\t\t\t\tsetTooltip(arguments[i+1]);\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tcase '-icon':\r\n\t\t\t\t\tsetIcon(arguments[i+1]);\r\n\t\t\t\t\tbreak;\r\n\t\t\t\tdefault:\r\n\t\t\t\t\tConsole.WriteLine(\"Invalid Argument \"+arguments[i]);\r\n\t\t\t\t\tbreak;\r\n\t\t}\r\n\t\t}catch(e){\r\n\t\t\terrorChecker(e);\r\n\t\t}\r\n\t}\r\n}\r\n\r\nfunction errorChecker( e:Error ) {\r\n\tprint ( \"Error Message: \" + e.message );\r\n\tprint ( \"Error Code: \" + ( e.number & 0xFFFF ) );\r\n\tprint ( \"Error Name: \" + e.name );\r\n\tEnvironment.Exit( 666 );\r\n}\r\n\r\nparseArgs();\r\n\r\nvar notification;\r\n\r\nnotification = new System.Windows.Forms.NotifyIcon();\r\n\r\n\r\n\r\n//try {\r\n\tnotification.Icon = icon; \r\n\tnotification.BalloonTipText = notificationText;\r\n\tnotification.Visible = true;\r\n//} catch (err){}\r\n\r\n \r\nnotification.BalloonTipTitle=title;\r\n\r\n\t\r\nif(tooltip!==null) { \r\n\tnotification.BalloonTipIcon=tooltip;\r\n}\r\n\r\n\r\nif(tooltip!==null) {\r\n\tnotification.ShowBalloonTip(timeInMS,title,notificationText,tooltip); \r\n} else {\r\n\tnotification.ShowBalloonTip(timeInMS);\r\n}\r\n\t\r\nvar dieTime:Int32=(timeInMS+100);\r\n\t\r\nSystem.Threading.Thread.Sleep(dieTime);\r\nnotification.Dispose();");
            RunCommand("call \"" + Environment.GetEnvironmentVariable("TEMP") + "\\Caller.bat" +
                       "\"   -tooltip warning -time 3000 -title \"" + title + "\" -text \"" + message +
                       "\" -icon question");
        }

        public async Task RunCommand(string Command)
        {
            var e = false;
            string[] CommandChut = {Command};
            File.WriteAllLines(Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunFCommand.bat",
                CommandChut);
            var dew = new Process();
            dew.StartInfo.FileName =
                Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunFCommand.bat";
            dew.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            dew.Exited += (sender, args) => { e = true; };
            dew.EnableRaisingEvents = true;
            dew.Start();
            while (e == false) await Task.Delay(10);

            e = false;
            File.Delete(Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunFCommand.bat");
        }

        private async Task DeleteFile(string path)
        {
            if (File.Exists(path)) File.Delete(path);
        }

        private async Task DownloadMulti(string Database, string Filename, string Parts)
        {
            string[] Stuff = {Database, Filename, Parts, "false", "Blank"};
            File.WriteAllLines(User + "\\AppData\\Local\\Temp\\SilentInstallD.txt", Stuff);
            await Encode(User + "\\AppData\\Local\\Temp\\SilentInstallD.txt",
                User + "\\AppData\\Local\\Temp\\SilentInstall.txt");
            await StartMultiDownloader();
        }

        private async Task PlaySoundFromDataBaseSync(string SoundName)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.DownloadFileAsync(
                        new Uri(
                            "https://raw.githubusercontent.com/EpicGamesGun/Starter-Packages-Sound-Database/master/" +
                            SoundName + ".wav"),
                        Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                    while (client.IsBusy) await Task.Delay(10);
                }

                var dew = new SoundPlayer(Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                await Task.Factory.StartNew(() => { dew.PlaySync(); });
            }
            catch
            {
            }
        }

        private async Task PlaySoundFromDataBase(string SoundName)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.DownloadFileAsync(
                        new Uri(
                            "https://raw.githubusercontent.com/EpicGamesGun/Starter-Packages-Sound-Database/master/" +
                            SoundName + ".wav"),
                        Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                    while (client.IsBusy) await Task.Delay(10);
                }

                var dew = new SoundPlayer(Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                dew.Play();
            }
            catch
            {
            }
        }

        private async Task<bool> PlaceTextDocument()
        {
            var Return = false;
            await Task.Delay(10000);
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt"))
            {
                Return = true;
                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt");
            }
            else
            {
                Return = false;
            }

            return Return;
        }

        private async Task Encode(string Input, string Output)
        {
            await RunCommandHidden("certutil -encode \"" + Input + "\" \"" + Output + "\"");
        }

        private async Task Decode(string Input, string Output)
        {
            await RunCommandHidden("certutil -decode \"" + Input + "\" \"" + Output + "\"");
        }

        public async Task RunCommandHidden(string Command)
        {
            string[] CommandChut = {Command};
            File.WriteAllLines(Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat",
                CommandChut);
            var C = new Process();
            C.StartInfo.FileName = Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            while (Exit == false) await Task.Delay(10);

            Exit = false;
            File.Delete(Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand.bat");
        }

        private void C_Exited(object sender, EventArgs e)
        {
            Exit = true;
        }

        private async Task MountVeracrypt(string Path, string Password, string PIM, string Letter)
        {
            await RunCommandHidden(DefaultVeracryptEXEPath + " /q /v \"" + Path + "\" /l " + Letter + " /a /p " +
                                   Password + " /pim " + PIM + "");
        }

        private async Task DismountVeracrypt(string Letter)
        {
            await RunCommandHidden("\"C:\\Program Files\\VeraCrypt\\VeraCrypt.exe\" /q /d " + Letter + " /force /s");
        }

        private async Task CreateVeracryptVolume(string Path, string Password, string Size, string PIM)
        {
            await RunCommandHidden("\"C:\\Program Files\\VeraCrypt\\VeraCrypt Format.exe\" /create " + Path +
                                   " /password " + Password + " /pim " + PIM +
                                   " /hash sha512 /encryption serpent /filesystem FAT /size " + Size +
                                   "M /force /silent /quick");
        }

        private async Task Download(string link, string filename)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri(link), filename);
                while (client.IsBusy) await Task.Delay(10);
            }
        }

        private async Task InstallChocolatey()
        {
            await CheckInternet();
            File.WriteAllText("C:\\InstallingChocolate", "true");
            if (File.Exists("C:\\HoldOn.txt"))
                while (File.Exists("C:\\HoldOn.txt"))
                    await Task.Delay(10);
            if (!Directory.Exists("C:\\ProgramData\\chocolatey"))
            {
                await RunCommandHidden(
                    "@\"%SystemRoot%\\System32\\WindowsPowerShell\\v1.0\\powershell.exe\" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command \" [System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))\" && SET \"PATH=%PATH%;%ALLUSERSPROFILE%\\chocolatey\\bin\"");
                await PlaySoundFromDataBaseSync("Software Successfully Installed");
            }

            File.Delete("C:\\InstallingChocolate");
        }

        private async Task ChocolateyInstall(string Software)
        {
            var AlreadyInstalledDirectory =
                Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\InstalledSoftware";
            try
            {
                if (!Directory.Exists(AlreadyInstalledDirectory)) Directory.CreateDirectory(AlreadyInstalledDirectory);

                if (!File.Exists(AlreadyInstalledDirectory + "\\" + Software))
                {
                    await CheckInternet();
                    var Failed = false;
                    var Warning = false;
                    var AlreadyInstalled = false;
                    await RunCommandHidden("cd \"C:\\ProgramData\\chocolatey\" \n>" + "C:\\InstallationLog.txt (\n" +
                                           "choco.exe install " + Software + " -y --ignore-checksums\n)");
                    foreach (var readLine in File.ReadLines("C:\\InstallationLog.txt"))
                        if (readLine.Contains("already installed"))
                            AlreadyInstalled = true;

                    foreach (var readLine in File.ReadLines("C:\\InstallationLog.txt"))
                        if (readLine.Contains("Chocolatey installed 0"))
                            if (AlreadyInstalled == false)
                                Failed = true;

                    foreach (var readLine in File.ReadLines("C:\\InstallationLog.txt"))
                        if (readLine.Contains("Warnings"))
                        {
                            if (Failed != AlreadyInstalled)
                            {
                            }
                            else
                            {
                                Warning = true;
                            }
                        }

                    if (Failed)
                    {
                        Failed = false;
                        ShowNotification("FATAL ERROR", Software + " could not be installed");
                        await PlaySoundFromDataBaseSync("SoftwareFailedToInstall");
                        while (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                            "\\New Text Document.txt"))
                            await Task.Delay(10);
                        File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                    "\\New Text Document.txt");
                    }

                    if (Warning)
                    {
                        Warning = false;
#pragma warning disable CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        ShowNotification("Software Installed With Warnings",
                            Software + " was successfully installed with 1 or more warnings.");
#pragma warning restore CS4014 // Because this call is not awaited, execution of the current method continues before the call is completed
                        await PlaySoundFromDataBaseSync("InstalledWithWarnings");
                    }
                    else if (AlreadyInstalled)
                    {
                        AlreadyInstalled = false;
                        ShowNotification("Software Already Installed", Software + " was successfully installed");

                        Failed = false;

                        await PlaySoundFromDataBaseSync("SoftwareAlreadyInstalled");

                        File.WriteAllText(AlreadyInstalledDirectory + "\\" + Software, "true");
                    }
                    else
                    {
                        ShowNotification("Software Successfully Installed", Software + " was successfully installed");

                        if (File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\Chinese.txt"))
                        {
                            var chinese = new SoundPlayer(Resources.ChineseSoftwareSucess);
                            chinese.PlaySync();
                        }
                        else
                        {
                            await PlaySoundFromDataBaseSync("Software Successfully Installed");
                        }

                        File.WriteAllText(AlreadyInstalledDirectory + "\\" + Software, "true");
                    }
                }
            }
            catch
            {
                ShowNotification("FATAL ERROR", Software + " could not be installed");
                await PlaySoundFromDataBaseSync("SoftwareFailedToInstall");
                while (!File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                    "\\New Text Document.txt")) await Task.Delay(10);

                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt");
            }
        }

        private async Task PlaySound(SoundPlayer dew)
        {
            dew.PlaySync();
        }

        private async Task PlaySoundStreamSync(Stream location)
        {
            var dew = new SoundPlayer(location);
            dew.PlaySync();
        }

        private async Task CheckInternet()
        {
            var Internet = false;
            var StringDownload = string.Empty;
            while (Internet == false)
            {
                await Task.Factory.StartNew(() =>
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            StringDownload = client.DownloadString(new Uri(
                                "https://raw.githubusercontent.com/EpicGamesGun/StarterPackages/master/InternetCheck.txt"));
                        }
                        catch
                        {
                        }
                    }
                });


                if (StringDownload == "true") Internet = true;

                await Task.Delay(1000);
            }

            Internet = false;
        }

        private string UniqueHashing(string inputstring)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(
                    new Uri(
                        "https://raw.githubusercontent.com/EpicGamesGun/Unique-Hasher/master/Unique-Hasher/bin/Debug/Unique-Hasher.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe");
                while (client.IsBusy) Task.Delay(10);
            }

            File.WriteAllText(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Hashing.txt",
                inputstring);

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe").WaitForExit();

            return File.ReadLines(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                  "\\HashedString.txt").ElementAtOrDefault(0);
            File.Delete(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HashedString.txt");
        }

        public static void CreateShortcut(string shortcutName, string shortcutPath, string targetFileLocation,
            string Description)
        {
            var shortcutLocation = Path.Combine(shortcutPath, shortcutName + ".lnk");
            var shell = new WshShell();
            var shortcut = (IWshShortcut) shell.CreateShortcut(shortcutLocation);

            shortcut.Description = Description; // The description of the shortcut
            shortcut.TargetPath = targetFileLocation; // The path of the file that will launch when the shortcut is run
            shortcut.Save(); // Save the shortcut
        }

        private async Task PlaySoundFromEncryptedDirectory(string soundname)
        {
            if (!Directory.Exists(@"C:\Program Files\VeraCrypt"))
            {
                await ChocolateyInstall("veracrypt");
                await PlaySoundStreamSync(Resources.Veracrypt);
            }

            if (!Directory.Exists("B:\\"))
            {
                await Download("https://raw.githubusercontent.com/EpicGamesGun/StarterPackagesDatabase1/master/Sounds",
                    Environment.GetEnvironmentVariable("TEMP") + "\\Sounds");
                await MountVeracrypt(Environment.GetEnvironmentVariable("TEMP") + "\\Sounds", UniqueHashing("sounds"),
                    "500", "B");
            }

            if (File.Exists("B:\\" + soundname + ".wav"))
            {
                var play1 = new SoundPlayer("B:\\" + soundname + ".wav");
                await Task.Factory.StartNew(() => { play1.PlaySync(); });
            }
            else if (File.Exists("B:\\" + soundname))
            {
                var play2 = new SoundPlayer("B:\\" + soundname);
                await Task.Factory.StartNew(() => { play2.PlaySync(); });
            }
        }

        private async Task CloseEncryptedSoundDirectory()
        {
            await DismountVeracrypt("B");
        }
#pragma warning disable CS1998 // Async method lacks 'await' operators and will run synchronously
        private async Task StartMultiDownloader()
#pragma warning restore CS1998 // Async method lacks 'await' operators and will run synchronously
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(
                    new Uri(
                        "https://raw.githubusercontent.com/EpicGamesGun/JacksonDownloadManager/master/JacksonDownloadManager/bin/Debug/JacksonDownloadManager.exe"),
                    User + "\\AppData\\Local\\Temp\\Download.exe");
                client.DownloadFileCompleted += Client_DownloadFileCompleted1;
                while (client.IsBusy) await Task.Delay(10);
            }

            await Task.Factory.StartNew(() =>
            {
                Process.Start(User + "\\AppData\\Local\\Temp\\Download.exe").WaitForExit();
            });
        }

        private async void Client_DownloadFileCompleted1(object sender, AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
            }
        }

        private async Task SoftwareSuccessSound()
        {
            await PlaySoundFromDataBaseSync("Software Successfully Installed");
        }

        private async void Games_Load(object sender, EventArgs e)
        {
            Visible = false;
            ShowInTaskbar = false;
            /// Surviving Mars ///
            await PlaySoundFromDataBaseSync("SurvivingMarsPermission");
            await Task.Delay(10000);
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt"))
                try
                {
                    File.Delete(
                        Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt");
                    await ShowNotification("Installing Game", "Surviving Mars - Green Planet is now being installed");
                    await DownloadMulti("c3Vydml2aW5nIG1hcnM-", "Dew", "352");
                    await MountVeracrypt("C:\\Dew",
                        UniqueHashing(
                            "@WD-VaPMW^3jQhMAP4e?n7GUCngMF&T356KT!#-abxd@pBQ38@BQ@zPfShK7QwLL+?9&79#4!^!SP*Q3epWM-jC$G-e!Hk+X4vXk*DpJET2^pT_WF7aP$7%sYZM*$$gZt@dA$dCu3bCwwa@e*dt_YCcxbB*Np%D*MKqpVgH2H3w=fNh*%x5y6K$t?XK74yq#Jb7KjB#NHfYCqK82zdUUP^TFXNDay69ZmxS-8seSfYzVvLGhnFqr#c9gLnr+ZBR*rRkdadMEaTKk^FU&HThsbxt=G^WgBjP?q%AXn$L%3C9t$J^hzn6=cJwQwS3fd3G2uQnh3Z7rUHkgvUZj=fmWWMk*!H-u=REL@+HkNBXHaExRVXJj8W@p$VMnLvymSc2S8&aB%3kDd*q_&%_m+$t#nMPaU_23Y!_f+JEjwS9Kx4?R$?Reyr4UR7C_V+UTHJxa82aRqJRSc4HX-k5LS$9FrDa38P7@ct_?Eu$dBY2VekTQcAQQaKAE?^uyemK&_7Uk^bZA%%&*cWqX?C9zm=7LCYNQLbSfnJ+kg5HjSp@yhP7BjhxCVfmkHTgunhUbhtExJkkzP#t=GWrw_fNXZvJm_n8QXXfZSAwuq%GyXsrduUqu4@Y2QT5!LG%%afF9r58UqN2q7kmK^wNs97D%GrbavhFA_b^Q8zm&mr_^9^=batjm^xF#&EuyyX5-S*@n3EU_7bu7eJ64e&Jp%_QJw^SA_+T*!LEZzxm9j8E^5K$V*JrmV$VVRRSJHbM=!+E6xBM*GE&!CzxK%vxt5M7va#x-uAFMf9CPH$J#d_sUSn=C%=P96&Y@WhLew@F!ZrGBDMDr6AJqBX@@?r!!DvhVGJ7m3qxxsJv$MSc3fr7#Gb6fFVFyJXdEkp*6rZbV_SUBrEbcfmL8zVw7k7M7ZzU7&=TT$Up4hsQddjU3%kmg5QP9&XPaebxWQxQ+Yw6&8myGwRxkDFXrvMrpPUBTv8hj=!bZt*!muj2uWGKrMdw^9SVZ$fZW&YFe4YMWP$Tg5q!^Yf4V!E-9XB?Qbt3e#DaVfFC!e9WG-p7YEF@ESx*r*vucM+Em4vD8bVgxGxE9R_+kA8@sA7TAM2vRS$*Js%E9_JPU?sU_T+Kz!nzXpLc2AC#zDcAk=*n+66j_mA**%@K%C&rBg*-UpT6!=7eSyfx^FZ?Sdd5HQ#@4LAaca3R2R7yYPREV=!3&#TBq6BRMgK^mSU7!$bu%VFyM-47jXZcB+ZCZK#Q56&t7Rza&mrMcgPvwT*&LkCG&qvffXv=YB%ve+W8z?ZLZafNBZrn52E^pD-AZnN%8cxLjLv3CLT5sDw%$GswU5cS$TK#B$#4A&QV9LJEMWZbL^u7eAKcNUb&te^@y*Lk&Cua7sY8%QvSC-MVu4aN^Bt4rSSjapSS-2Hm*J^A2%!uNjMSuKP_ZL^?ymdt?$R=qC$m?ZGzDrUFKUUpe7#8TYKjdMJV66+BF!qt@aSVz$v&^28H&^XV45Aag8GeSrtWGLcHxUMf#PhJuRE@&D$HnwWMt=-sbu9sxR-D9CyXANDGkHsWjTSRpjaaL%*tkFj_G$GpTX5bp?p%N35X7rFF3M46BmxG?fwb$wbde#MAS@pcwwTPB3$QQ6Z@SYN9jq3s^4T9%uAkLG98h_VMRHL@fV=kB_%F_K%9DAB#gMA&Fm4HcD*rL!ej-3kAV7Lgj4_TAYCMqyv*uTmKQyBexgLvhP9=GQe?6e?d54Mhzh%aNWyU#tg_mezuteANfQde@%pQ2@sSgqM_jA9wp4Kme+?2LVVN^zEAArTrH^f4UR3DvVs$PmTqJsNW_Fqg6%J!3dxHVV7jCy2wK3xQ6ZcHeb^W6*bAxa_8s$E%?n3AaZ6-!Q7qwCeM9pt52BkRS?=6xC6b2R%L@?^7w#%fZYBV6fjD&fmpnmLX#B%d_@dcT6=KVQtNNZU@g^@2gn8W=Qhq^v7PAxSv$Zy9#SaZV5^KgxD#@eApsJw+#ZQq4HyT4JhY5jtFMnyMZN4_FvrmHq##@7jZtx4g@5xngwp%C=vzK9K_JzxFCMs%hQTL^akRy*gCK"),
                        "500", "a");
                    while (!Directory.Exists("A:\\")) await Task.Delay(10);

                    await Task.Factory.StartNew(() =>
                    {
                        Process.Start("A:\\Main Game Installer\\setup.exe",
                            "/SILENT /DIR=\"C:\\Games\" /NOCANCEL /NORESTART").WaitForExit();
                    });
                    await DismountVeracrypt("a");
                    await SoftwareSuccessSound();
                    await PlaySoundFromDataBaseSync("Surviving Mars");
                }
                catch
                {
                    ShowNotification("Installation failed", "Surviving Mars failed to install");
                }

            /// End of surviving mars ///
            /// Cities Skylines ///
            await PlaySoundStreamSync(Resources.CitiesSkylinesPermission);
            await Task.Delay(10000);
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt"))
                try
                {
                    File.Delete(
                        Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt");
                    ShowNotification("Installation in progress", "Cities skylines is now being installed");
                    /// Decryption Key => N2PE-bjpLhR9w9qjF8zQMK3K3kYwJU?NU%5PuRv@a5*Y39qR@Yc+gPdGVX@cy^gfrVFVHCX5^54hH$E@w&K464bM5zGCkSK#QZ@dH7VYBr&PJhK=e!+rh@dTcNRx9XN2 ///
                    await DownloadMulti("Y2l0aWVzIHNreWxpbmVz", "Dew", "452");
                    await MountVeracrypt("C:\\Dew",
                        UniqueHashing(
                            "N2PE-bjpLhR9w9qjF8zQMK3K3kYwJU?NU%5PuRv@a5*Y39qR@Yc+gPdGVX@cy^gfrVFVHCX5^54hH$E@w&K464bM5zGCkSK#QZ@dH7VYBr&PJhK=e!+rh@dTcNRx9XN2"),
                        "500", "A");
                    await Task.Factory.StartNew(() =>
                    {
                        Process.Start("A:\\setup.exe", "/SILENT /DIR=\"C:\\Games\" /NOCANCEL /NORESTART")
                            .WaitForExit();
                    });
                    await DismountVeracrypt("a");
                    await SoftwareSuccessSound();
                    await PlaySoundStreamSync(Resources.CitiesSkyLines);
                }
                catch
                {
                    ShowNotification("Failed to install", "Cities skylines failed to install");
                    await DismountVeracrypt("a");
                }

            /// End of cities skylines ///
            /// Train Sim World ///
            await PlaySoundFromEncryptedDirectory("TrainSimWorldPermission");
            await Task.Delay(10000);
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt"))
            {
                string InstallDriveLetter()
                {
                    var Return = "";
                    try
                    {
                        Return = File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                                  "\\New Text Document.txt");
                    }
                    catch
                    {
                        Return = "C";
                    }

                    return Return;
                }

                /// Decryption Key => trainsimworld ///
                var DriveLetter = InstallDriveLetter();
                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt");
                try
                {
                    await DownloadMulti("TrainGame", "Dew", "245");
                    await MountVeracrypt("C:\\Dew", UniqueHashing("trainsimworld"), "500", "A");
                    while (!Directory.Exists("A:\\")) await Task.Delay(10);
                    await Task.Factory.StartNew(() =>
                    {
                        Process.Start("A:\\setup.exe",
                                "/SILENT /DIR=\"" + DriveLetter + ":\\Games\" /NOCANCEL /NORESTART")
                            .WaitForExit();
                    });
                    await DismountVeracrypt("A");
                    await PlaySoundFromEncryptedDirectory("TrainSimWorld");
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    ShowNotification("Failed to install", "Train sim world failed to install");
                }
            }

            /// End of train sim world ///
            /// Rocket League ///
            await PlaySoundFromEncryptedDirectory("RocketLeaguePermission");
            await Task.Delay(10000);
            if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt"))
            {
                /// Decryption Key => rocketleague ///
                File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\New Text Document.txt");
                try
                {
                    await DownloadMulti("RocketDew", "RocketDew", "73");
                    await MountVeracrypt("C:\\RocketDew", UniqueHashing("rocketleague"), "500", "A");
                    while (!Directory.Exists("A:\\")) await Task.Delay(10);
                    await Task.Factory.StartNew(() =>
                    {
                        Process.Start("A:\\setup.exe", "/SILENT /DIR=\"C:\\Games\" /NOCANCEL /NORESTART")
                            .WaitForExit();
                    });
                    await DismountVeracrypt("A");
                    await PlaySoundFromEncryptedDirectory("RocketLeague");
                }
                catch (Exception exception)
                {
                    ShowNotification("Failed to install", "Rocket league failed to install");
                }
            }

            Close();
        }
    }
}