﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using UsefulTools;

namespace StarterPackages
{
    public class VisualStudioInstallerBackground
    {
        public bool IsBusy { get; set; }
        BackgroundWorker Installer = new BackgroundWorker();

        private async Task Download(string link, string path)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri(link), path);
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
        }
        public async Task DoWork()
        {
            // https://raw.githubusercontent.com/RytheProtogen/VisualStudioInstaller/master/Visual%20Studio.part01.exe //
            Installer.DoWork += async (sender, args) =>
            {
                IsBusy = true;
                int Parts = 54;
                Directory.CreateDirectory(Environment.GetEnvironmentVariable("APPDATA") + "\\VisualStudioInstaller");
                string MainDirectory = Environment.GetEnvironmentVariable("APPDATA") + "\\VisualStudioInstaller";
                await Download(
                    "https://raw.githubusercontent.com/RytheProtogen/VisualStudioInstaller/master/Visual%20Studio.part01.exe",
                    MainDirectory + "\\Visual Studio.part01.exe");
                for (int i = 0; i <= Parts; i++)
                {
                    if (i < 10)
                    {
                        await Download(
                                        "https://raw.githubusercontent.com/RytheProtogen/VisualStudioInstaller/master/Visual%20Studio.part0" +
                                        i.ToString() + ".rar", MainDirectory + "\\Visual Studio.part0" + i.ToString() + ".rar");
                    }
                    else if(i > 9)
                    {
                        await Download(
                            "https://raw.githubusercontent.com/RytheProtogen/VisualStudioInstaller/master/Visual%20Studio.part" +
                            i.ToString() + ".rar", MainDirectory + "\\Visual Studio.part" + i.ToString() + ".rar");
                    }
                }
                string DecryptionKey = new WebClient().DownloadString("https://raw.githubusercontent.com/RytheProtogen/VisualStudioInstaller/master/DecryptionKey.txt");
                DecryptionKey = UsefulTools.Encryption.UniqueHashing(DecryptionKey);
                var j = 0;
                var h = 0;
                var k = 0;
                DirectoryInfo DirectoryDecrypt = new DirectoryInfo(MainDirectory);
                foreach (var fileInfo in DirectoryDecrypt.GetFiles())
                {
                    j++;
                }
                foreach (var fileInfo in DirectoryDecrypt.GetFiles())
                {
                    BackgroundWorker Decryption = new BackgroundWorker();
                    Decryption.DoWork += (o, eventArgs) =>
                    {
                        EncryptionAPI DecryptorHa = new EncryptionAPI();
                        Random d = new Random();
                        string RandomNumber = d.Next(1111, 9999).ToString();
                        DecryptorHa.FileDecrypt(fileInfo.FullName, "C:\\TEMP" + RandomNumber, DecryptionKey);
                        File.Delete(fileInfo.FullName);
                        File.Move("C:\\TEMP" + RandomNumber, fileInfo.FullName);
                        h++;
                    };
                    Decryption.RunWorkerAsync();
                    await Task.Delay(100);
                }
                while (h < j)
                {
                    await Task.Delay(10);
                }

                Process.Start(MainDirectory + "\\Visual Studio.part01.exe").WaitForExit();
                File.WriteAllText(MainDirectory + "\\AlreadyInstalled.txt","true");
                IsBusy = false;
            };
            Installer.RunWorkerCompleted += (sender, args) =>
            {

            };
            Installer.RunWorkerAsync();
        }
    }
}
