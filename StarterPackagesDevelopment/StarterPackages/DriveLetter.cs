﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StarterPackages
{
    public class DriveLetter
    {
        private char[] getAvailableDriveLetters()
        {
            List<char> availableDriveLetters = new List<char>() { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

            DriveInfo[] drives = DriveInfo.GetDrives();

            for (int i = 0; i < drives.Length; i++)
            {
                availableDriveLetters.Remove((drives[i].Name).ToLower()[0]);
            }

            return availableDriveLetters.ToArray();
        }

        public string GetRandomDriveLetter1()
        {
            string Return = "";
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                Console.WriteLine(c);
                Return = c.ToString();
            }

            return Return;
        }

        public string GetRandomDriveLetter2()
        {
            var i = 0;
            string Return = "";
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                if (i == 2)
                {
                    Return = c.ToString();
                }
                i++;
            }

            return Return;
        }

        public string GetRandomDriveLetter3()
        {
            string Return = "";
            var i = 0;
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                if (i == 3)
                {
                    Return = c.ToString();
                }

                i++;
            }

            return Return;
        }
    }
}
